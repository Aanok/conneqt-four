#ifndef BOARDGUI_H
#define BOARDGUI_H

#include "gameengine.h"
#include "fallingtoken.h"
#include "gui.h"
#include <iostream>
#include <QtOpenGL>
class FallingToken;
class GUI;

#define CELLSIZE 100
#define SPRITESIZE 256

#define STATE_READY 0 // Idle, responsive to user input
#define STATE_ADDING 1 // Press event locked location of token to add, waiting for Release
#define STATE_BUSY 2 // Unresponsive

#define CELLTRAVERSETIME 17000
#define SHAKINGTIME 350

class BoardGUI : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT

public:
    BoardGUI(const bool m_shakingAnim = true, const bool postprocessing = true, GameEngine *game = 0, GUI *parent = 0);

    void reset();
    void enableShaking(const bool value);
    void enablePostprocessing(const bool value);
    void setSFXVolume(const qreal vol);

private:
    GameEngine *m_game;
    FallingToken *m_fallingtokp1, *m_fallingtokcpu;
    unsigned short int m_guistate, m_fallentokens;
    int m_hovercol, m_hoverrow;
    bool m_shakingenabled, m_ppenabled;
    QPropertyAnimation *m_shakingAnim;
    QOpenGLVertexArrayObject m_vao;
    QOpenGLBuffer m_vbo; // Vertex buffer
    QOpenGLBuffer m_ebo; // Element buffer
    QOpenGLShaderProgram *m_vhsrenderer, *m_blurrenderer, *m_mainrenderer;
    QOpenGLTexture *m_textokp1, *m_textokcpu, *m_textokhover, *m_texboard;
    GLuint m_fbo[3], m_fbotex[3]; // Framebuffers, one used for main rendering, two for blur passes

    QSoundEffect m_fallsfxp1, m_fallsfxcpu;

    void genTokSprite(const QColor color, QOpenGLTexture *&outTex);
    void drawBoundSprite(const QRectF dest);
    void setupFBO();
    void addToken();
    void initShakingAnimation();

signals:
    void CommandCPUMove();

private slots:
    void OnMadeCPUMove(int row, int col);
    void OnFallingTokP1Finished();
    void OnFallingTokCPUFinished();
    void onShakingFinished();

public slots:
    void OnTimeout();

protected:
    void initializeGL() Q_DECL_OVERRIDE;
    void paintGL() Q_DECL_OVERRIDE;
    void resizeEvent(QResizeEvent *e) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QMouseEvent *) Q_DECL_OVERRIDE;
    void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;
    void keyReleaseEvent(QKeyEvent *event) Q_DECL_OVERRIDE;
};

#endif // BOARDGUI_H
