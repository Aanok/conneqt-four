#ifndef ALERT_H
#define ALERT_H

#include "menuitems.h"
#include "utils.h"
#include <QtWidgets>

#define ALERTMARGIN 25

enum AlertButton { Yes = 0, No = 1, None = 2 };

class AlertChoice : public QWidget
/* Class for a choice in a generic Alert pop up. Draws a small
 * arrow when highlighted.
 * The actual highlighting is deferred to the Alert object, since
 * we want to be able to control the popup with keyboard too, so
 * simply setting a boolean on enter/leave events isn't enough. */
{
    Q_OBJECT

public:
    AlertChoice(const QString &text, const bool defaultHighlighted = false);

    void setHighlighted(bool h);
    bool isHighlighted() const;

private:
    QString m_text;
    QPolygon m_arrow;
    bool m_highlighted;
    int m_textwidth, m_textheight, m_arrowwidth;

signals:
    void clicked();
    void needHighlighting(bool val);

protected:
    void paintEvent(QPaintEvent *) Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QMouseEvent *) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QMouseEvent *) Q_DECL_OVERRIDE;
    void enterEvent(QEvent *) Q_DECL_OVERRIDE;
    void leaveEvent(QEvent *) Q_DECL_OVERRIDE;
};


class Alert : public QWidget
/* Class for a pop up alert with a message and a Yes/No choice.
 * The message can be on one or two lines (use an empty subText for the latter).
 * Basically a more rigid QMessageBox with custom looks. */
{
    Q_OBJECT

public:
    Alert(const QString &topText, const QString &subText = QString(),
          const AlertButton defaultHovered = AlertButton::None,
          QWidget *parent = 0);
    Alert(const QString &topText,
          const AlertButton defaultHovered = AlertButton::None,
          QWidget *parent = 0);

private:
    AlertChoice *yesbutt, *nobutt;
    QPen m_pen;

signals:
    void yesClicked();
    void noClicked();

private slots:
    void onYesClicked();
    void onNoClicked();
    void onYesEnteredOrLeft(bool val);
    void onNoEnteredOrLeft(bool val);

protected:
    void paintEvent(QPaintEvent *) Q_DECL_OVERRIDE;
    void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;
    void keyReleaseEvent(QKeyEvent *event) Q_DECL_OVERRIDE;
};

#endif // ALERT_H
