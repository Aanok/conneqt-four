#include "menuitems.h"
#include <iostream>

// SLIDER
Slider::Slider(const int def, const int min, const int max, QWidget *parent)
    : QWidget(parent)
{
    m_value = def;
    m_minbound = min;
    m_maxbound = max;
    m_tracking = false;

    // Setup brushes
    m_idleBrush = QBrush(this->palette().foreground());
    m_trackingBrush = QBrush(this->palette().highlight());
    m_pen = QPen(this->palette().foreground().color().darker(200)); // HSV V halved

    // Dimensions. Widget as tall as head, which is as tall as the font.
    m_dim = QWidget::fontMetrics().height();
    setMinimumHeight(m_dim);

    // Width the head will actually be able to move through
    m_effectiveWidth = this->width() - m_dim*0.5;

    Utils::addGlowToWidget(this);
}


void Slider::updateValueFromMouseX(const int x)
/* Projects x over the rail to a clamped value. */
{
    int newval = x * (m_maxbound - m_minbound) / m_effectiveWidth;
    newval = qBound(m_minbound, newval, m_maxbound);
    if (newval != m_value) {
        // It's a new value, update and emit signal
        m_value = newval;
        emit valueChanged(newval);
    }
}


void Slider::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    // Draw the rail
    p.fillRect(0,(this->height()-(m_dim*0.1))/2,this->width(),
               m_dim*0.1, m_idleBrush);

    // Draw the head
    p.setBrush(m_tracking ? m_trackingBrush : m_idleBrush);
    p.setPen(m_pen);
    // Note the -1 because of Qt's box model
    p.drawRect(m_effectiveWidth*0.01*m_value-1,(this->height()-m_dim)/2,
               m_dim*0.5, m_dim);
}


void Slider::mousePressEvent(QMouseEvent *event)
/* On mousePress, we move the head to the cursor and start tracking. */
{
    m_tracking = true;
    updateValueFromMouseX(event->x());
}


void Slider::mouseMoveEvent(QMouseEvent *event)
/* On mouseMove, if tracking we update the value. */
{
    if (m_tracking) {
        updateValueFromMouseX(event->x());
    }
}


void Slider::mouseReleaseEvent(QMouseEvent *)
/* On mouseRelease, we stop tracking. */
{
    m_tracking = false;
}


void Slider::resizeEvent(QResizeEvent *event)
/* On resizeEvent, we need to update m_effectiveWidth, then proceed as usual. */
{
    m_effectiveWidth = event->size().width() - m_dim*0.5;
    QWidget::resizeEvent(event);
}


// MENUBUTTON
MenuButton::MenuButton(const QString &text, QWidget *parent)
    : QWidget(parent)
{
    m_pen = QPen(QColor(94, 8, 8));
    m_pen.setWidth(3);
    m_brushIdle = QBrush(QColor(142, 11, 11));
    m_brushHovered = QBrush(QColor(189, 15, 15));
    m_isHovered = false;

    /* Set a minimum size such that there are 15,15 horizontal
     * margins and 5,5 vertical margins around the text. */
    setMinimumSize(QWidget::fontMetrics().width(text)+30,
                   QWidget::fontMetrics().height()+10);

    /* The text itself we wrap in a label and layout. This way it can have
     * a glow effect while the button does not. */
    QHBoxLayout *layout = new QHBoxLayout();
    BloomedLabel *label = new BloomedLabel(text);
    layout->addWidget(label);
    layout->setAlignment(label, Qt::AlignCenter);
    this->setLayout(layout);
}


void MenuButton::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    p.setPen(m_pen);
    p.setBrush(m_isHovered ? m_brushHovered : m_brushIdle);
    // Note the -1 because of Qt's box model
    p.drawRect(0,0, this->width()-1, this->height()-1);
}


void MenuButton::mousePressEvent(QMouseEvent *)
/* For a better feel to the click. */
{
    m_isHovered = false;
}


void MenuButton::mouseReleaseEvent(QMouseEvent *event)
{
    if (this->rect().contains(event->pos())) {
        // The cursor is still inside the button, so we restore the hovering.
        m_isHovered = true;
    }
    emit clicked();
}


void MenuButton::enterEvent(QEvent *)
/* Triggers hovering. */
{
    m_isHovered = true;
}


void MenuButton::leaveEvent(QEvent *)
/* Ends hovering. */
{
    m_isHovered = false;
}



// BLOOMEDLABEL
BloomedLabel::BloomedLabel(QString text)
    : QLabel(text)
{
    Utils::addGlowToWidget(this);
}

BloomedLabel::BloomedLabel()
    : QLabel()
{
    Utils::addGlowToWidget(this);
}



// VALUE SLIDESHOW
ArrowButton::ArrowButton(bool facingleft, QWidget *parent)
    : QPushButton(parent)
{
    this->m_facingleft = facingleft;
    makeArrow();
    m_brush = this->palette().foreground();

    // Maximum width is fixed
    this->setMaximumWidth(ARROWBUTTONMAXWIDTH);

    Utils::addGlowToWidget(this);
}


void ArrowButton::makeArrow()
/* Populates the m_arrow QPolygon with points computed from the current
 * widget size. */
{
    m_arrow.clear();
    if (m_facingleft) {
        m_arrow << QPoint(this->width()/3, this->height()/2)
                 << QPoint(this->width(), this->height()/4)
                 << QPoint(this->width(), this->height()*3/4);
    } else {
        m_arrow << QPoint(this->width()*2/3, this->height()/2)
                 << QPoint(0, this->height()/4)
                 << QPoint(0, this->height()*3/4);
    }
}


void ArrowButton::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setPen(Qt::NoPen);
    painter.setBrush(m_brush);
    painter.drawPolygon(m_arrow);

}


void ArrowButton::enterEvent(QEvent *)
/* On enter, we switch to the highlight brush. */
{
    m_brush = this->palette().highlight();
}


void ArrowButton::leaveEvent(QEvent *)
/* On leave, we go back to the foreground brush. */
{
    m_brush = this->palette().foreground();
}


void ArrowButton::resizeEvent(QResizeEvent *event)
/* On resize, we process the event normally then recompute m_arrow. */
{
    QPushButton::resizeEvent(event);
    makeArrow();
}


ValueSlideshow::ValueSlideshow(QWidget *parent)
    : QWidget(parent)
{
    m_values = new QVector<int>();
    m_index = -1;
    m_displaytext = new BloomedLabel();
    m_displaytext->setAlignment(Qt::AlignCenter);
    QHBoxLayout *layout = new QHBoxLayout();
    ArrowButton *leftbutton = new ArrowButton(true);
    QObject::connect(leftbutton, SIGNAL(clicked(bool)), this, SLOT(onLeftArrowClicked()));
    ArrowButton *rightbutton = new ArrowButton(false);
    QObject::connect(rightbutton, SIGNAL(clicked(bool)), this, SLOT(onRightArrowClicked()));

    layout->addWidget(leftbutton);
    layout->setAlignment(leftbutton, Qt::AlignLeft);
    layout->addWidget(m_displaytext);
    layout->setAlignment(m_displaytext, Qt::AlignCenter);
    layout->addWidget(rightbutton);
    layout->setAlignment(rightbutton, Qt::AlignRight);

    this->setLayout(layout);
}


void ValueSlideshow::addValue(const int val)
/* Public method to register a new legal value. Also takes care of sizing. */
{
    m_values->append(val);
    // Ensure the label is big enough to display the value
    int w = QWidget::fontMetrics().width(QString().setNum(val))/2;
    if (w + 2*ARROWBUTTONMAXWIDTH > m_displaytext->minimumWidth()) {
        m_displaytext->setMinimumWidth(w + 2*ARROWBUTTONMAXWIDTH);
    }
    if (m_index == -1) {
        // First value inserted, we put it as default displayed.
        m_index = 0;
        m_displaytext->setNum(m_values->at(m_index));
    }
}


bool ValueSlideshow::setLabel(const int val, QString text)
/* EFFECT: sets a QString label to be displayed instead of the integer for a value
 * currently stored in the slideshow.
 * RETURNS true on success (val is valid), false on failure. */
{
    if (m_values->contains(val)) {
        m_labels[val] = text;
        // Ensure the label is big enough to display the text
        int w = QWidget::fontMetrics().width(text)/2;
        if (w + 2*ARROWBUTTONMAXWIDTH > m_displaytext->minimumWidth()) {
           m_displaytext->setMinimumWidth(w + 2*ARROWBUTTONMAXWIDTH);
        }
        if (val == m_values->at(m_index)) {
            // It's the currently displayed value, so we update displaytext right away
            m_displaytext->setText(text);
        }
        return true;
    } else {
        return false;
    }
}


bool ValueSlideshow::setCurrentValue(const int val)
/* EFFECT: changes currently selected value to val, if val is valid.
 * RETURNS true if val is valid, false otherwise. */
{
    int i = m_values->indexOf(val);
    if (m_values->contains(val) == -1) {
        // Invalid value
        return false;
    } else {
        // Valid value
        m_index = i;
        updateLabel();
        emit valueChanged(val);
        return true;
    }
}


void ValueSlideshow::updateLabel()
/* Looks up if the current value has a label; if it does, it puts the label
 * on display, otherwise display shows the current value. */
{
    QHash<int, QString>::const_iterator i = m_labels.find(m_values->at(m_index));
    if (i != m_labels.end()) {
        // Found label, use it
        m_displaytext->setText(i.value());
    } else {
        // No label known, display value
        m_displaytext->setNum(m_values->at(m_index));
    }
}


void ValueSlideshow::onLeftArrowClicked()
{
    // Unless we're at the bottom already, reduce index, update and signal.
    if (m_index > 0) {
        m_index--;
        updateLabel();
        emit valueChanged(m_values->at(m_index));
    }
}


void ValueSlideshow::onRightArrowClicked()
{
    // Unless we're at the top already, increase index, update and signal.
    if (m_index < m_values->length()-1) {
        m_index++;
        updateLabel();
        emit valueChanged(m_values->at(m_index));
    }
}



// CHECKBOX
CheckBoxBox::CheckBoxBox(const bool checked)
    : QWidget()
{
    m_pen = QPen(this->palette().foreground(), 2);
    m_penHighlighted = QPen(this->palette().highlight(), 2);
    m_checked = checked;
    m_highlighted = false;

    // We'll have a fixed size for this
    resize(20, 20);
    setMinimumSize(20,20);
    setMaximumSize(20,20);

    Utils::addGlowToWidget(this);
}


void CheckBoxBox::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    painter.setRenderHint(QPainter::Antialiasing);
    painter.setBrush(Qt::NoBrush);
    painter.setPen(m_highlighted ? m_penHighlighted : m_pen);
    // Draw box
    painter.drawRect(1,1, this->width()-m_pen.width(), this->height()-m_pen.width());
    // Draw cross
    if (m_checked) {
        painter.drawLine(this->width()*0.2,this->height()*0.2,
                         this->width()*0.8,this->height()*0.8);
        painter.drawLine(this->width()*0.8,this->height()*0.2,
                         this->width()*0.2,this->height()*0.8);
    }
}


void CheckBoxBox::mouseReleaseEvent(QMouseEvent *)
/* On mouseRelease, we toggle m_checked and signal. */
{
    if (m_checked) {
        m_checked = false;
        emit checkedChanged(Qt::Unchecked);
    } else {
        m_checked = true;
        emit checkedChanged(Qt::Checked);
    }
}


void CheckBoxBox::enterEvent(QEvent *)
/* On enter, we set the highlight. */
{
    m_highlighted = true;
}


void CheckBoxBox::leaveEvent(QEvent *)
/* On leave, we reset the highlight. */
{
    m_highlighted = false;
}

CheckBox::CheckBox(QString text, const bool checked)
    : QWidget()
{
    QHBoxLayout *layout = new QHBoxLayout();

    BloomedLabel *label = new BloomedLabel(text);
    CheckBoxBox *box = new CheckBoxBox(checked);
    connect(box, SIGNAL(checkedChanged(int)), this, SLOT(onCheckedChanged(int)));
    layout->addWidget(label);
    layout->setAlignment(label, Qt::AlignLeft);
    layout->addWidget(box);
    layout->setAlignment(box, Qt::AlignRight);

    this->setLayout(layout);
}


void CheckBox::onCheckedChanged(int value)
/* Since the CheckBoxBox is private and internal, we propagate the signal. */
{
    emit stateChanged(value);
}
