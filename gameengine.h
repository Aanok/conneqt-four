#ifndef GAMEENGINE_H
#define GAMEENGINE_H

#include "utils.h"
#include <climits> // INT_MAX, INT_MIN
#include <cstring> // memset
#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QtGlobal>

#define ROWS 6
#define COLS 7
#define TOKEMPTY 0 // 00
#define TOKP1 1 // 01
#define TOKP2 2 // 10
#define TIEVAL -1

#define STATE_P1WIN 0
#define STATE_P2WIN 1
#define STATE_TIE 2
#define STATE_RUNNING 3
#define STATE_QUITTING 4


class GameEngine : public QThread
{
    Q_OBJECT

public:
    GameEngine();
    int GetTok(const int row, const int col);
    void MakeP1Move(const int row, const int col);
    int GetFirstFreeRow(const int j);
    std::pair<int,int> MakeCPUMove();
    int GetGameState();
    void ResetGame();
    void QuitGame();
    void setMaxplies(const int value);

signals:
    void StateChanged(int state);
    void MadeCPUMove(int row, int col);

private slots:
    void OnCommandCPUMove();

private:
    void FinalizeGameReset();
    void SetState(const int state);
    void AdvanceTurn();
    int GetFirstFreeWorkRow(const int j);
    int GetWorkTok(const int row, const int col);
    void SetWorkTok(const int row, const int col, const int tok);
    void SetTok(const int row, const int col, const int tok);
    bool IsWinMove(const int row, const int col);
    int PairHeuristic();
    int MinMove(int a, int b, int ply);
    int MaxMove(int a, int b, int ply);

    int *m_board, *m_workboard;
    int m_maxplies, m_gameturn;
    int m_gamestate;

    QMutex m_mutex;
    QWaitCondition m_condition;

protected:
    void run() Q_DECL_OVERRIDE;
};

#endif // GAMEENGINE_H
