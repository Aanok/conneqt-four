#ifndef FALLINGTOKEN_H
#define FALLINGTOKEN_H

#include "boardgui.h"
#include <QtWidgets>

/* Conveniency class to animate a falling token, used in BoardGUI. */
class FallingToken : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int x READ x WRITE setX)
    Q_PROPERTY(int y READ y WRITE setY)

public:
    FallingToken(const int x, const int y, const int endy, const int falltime);

    int x() const;
    void setX(const int x);
    int y() const;
    void setY(const int y);
    int row, col;

    QPropertyAnimation *animation;

private:

    int m_x;
    int m_y;
};

#endif // FALLINGTOKEN_H
