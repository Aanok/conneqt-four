#ifdef DEBUG
#include "debug.h"

Debug::Debug(QWidget *parent)
    : QWidget(parent)
{
    m_lasttime = QTime::currentTime().msecsSinceStartOfDay();
    m_textpen = QPen(Qt::white);
    m_textfont.setPixelSize(30);
    this->resize(200,50);
}


void Debug::paintEvent(QPaintEvent *)
{
    int time = QTime::currentTime().msecsSinceStartOfDay();
    QPainter painter(this);
    painter.setPen(m_textpen);
    painter.setFont(m_textfont);
    painter.drawText(0,40, QString().setNum(time - m_lasttime));
    m_lasttime = time;
}

#endif // DEBUG
