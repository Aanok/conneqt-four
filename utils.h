#ifndef UTILS_H
#define UTILS_H

#include "gameengine.h"
#include <QWidget>
#include <QGraphicsDropShadowEffect>
#include <QJsonDocument>
#include <QJsonObject>
#include <QSize>
#include <QFile>
#include <QFileInfo>
#include <QApplication>
class GameEngine; // Forward declaration again, oh boy

/* In this header we find general utility classes for the application. */


/* This class wraps settings for the application, as well as methods for
 * reading and writing them to a Json object. */
class Settings
{
public:
    Settings();
    Settings(const QJsonObject &o);

    int difficulty, maxFPS, musicVolume, sfxVolume;
    bool fullscreen, shaking, postProcessing, maximized;
    QSize windowSize;

    QJsonObject toJson();
};


/* This class is a collection of program-wide utility functions.
 * WARNING: to be considered invalid until init() is run! */
class Utils
{
public:
    virtual void makeAbstract() = 0; // We don't want instances of this class.

    static Settings appSettings;

    static QGraphicsDropShadowEffect* addGlowToWidget(QWidget *target);
    static QString cropFilenameExtension(const QString filename);
    static void init();
    static void cleanup(GameEngine *ge);

private:
    static void readSettings();
    static void writeSettings();
};
#endif // UTILS_H
