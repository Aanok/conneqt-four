#version 110
#define radius 1.25
varying vec2 texCoord;
uniform sampler2D tex;
uniform vec2 texSize;
uniform vec2 dir;

void main() {
    vec4 sum = vec4(0.0);

    //dir --the direction of our blur:
    //(1.0, 0.0) -> x-axis blur
    //(0.0, 1.0) -> y-axis blur
    float hstep = dir.x * radius / texSize.x;
    float vstep = dir.y * radius / texSize.y;

    //apply blurring, using a 9-tap filter with predefined gaussian weights
    sum += texture2D(tex, vec2(texCoord.x - 4.0*hstep, texCoord.y - 4.0*vstep)) * 0.0162162162;
    sum += texture2D(tex, vec2(texCoord.x - 3.0*hstep, texCoord.y - 3.0*vstep)) * 0.0540540541;
    sum += texture2D(tex, vec2(texCoord.x - 2.0*hstep, texCoord.y - 2.0*vstep)) * 0.1216216216;
    sum += texture2D(tex, vec2(texCoord.x - 1.0*hstep, texCoord.y - 1.0*vstep)) * 0.1945945946;

    sum += texture2D(tex, vec2(texCoord.x, texCoord.y)) * 0.2270270270;

    sum += texture2D(tex, vec2(texCoord.x + 1.0*hstep, texCoord.y + 1.0*vstep)) * 0.1945945946;
    sum += texture2D(tex, vec2(texCoord.x + 2.0*hstep, texCoord.y + 2.0*vstep)) * 0.1216216216;
    sum += texture2D(tex, vec2(texCoord.x + 3.0*hstep, texCoord.y + 3.0*vstep)) * 0.0540540541;
    sum += texture2D(tex, vec2(texCoord.x + 4.0*hstep, texCoord.y + 4.0*vstep)) * 0.0162162162;

    gl_FragColor = sum;
}