#include "alert.h"

AlertChoice::AlertChoice(const QString &text, const bool defaultHighlighted)
    : QWidget()
{
    m_textwidth = QWidget::fontMetrics().width(text);
    m_textheight = QWidget::fontMetrics().height();
    m_highlighted = defaultHighlighted;
    this->m_text = text;

    m_arrow << QPoint(0, m_textheight/2)
          << QPoint(0,m_textheight)
          << QPoint(m_textheight/2, m_textheight*0.75);

    setMouseTracking(true);

    setMinimumWidth(m_textwidth + m_textheight);

    Utils::addGlowToWidget(this);
}


void AlertChoice::setHighlighted(bool h)
{
    m_highlighted = h;
}


bool AlertChoice::isHighlighted() const
{
    return m_highlighted;
}


void AlertChoice::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.setBrush(this->palette().foreground());
    painter.translate(0, (this->height() - m_textheight)/2);
    if (m_highlighted) {
        // Draw the arrow
        painter.translate((this->width()-m_textwidth-m_textheight)/2, 0);
        painter.drawPolygon(m_arrow);
        painter.translate(m_textheight,0);
    } else {
        painter.translate((this->width()-m_textwidth)/2, 0);
    }
    painter.setRenderHint(QPainter::Antialiasing);
    painter.drawText(0,m_textheight, m_text);
}


void AlertChoice::enterEvent(QEvent *)
/* Tells the Alert object we'd like to be highlited. */
{
    emit needHighlighting(true);
}


void AlertChoice::leaveEvent(QEvent *)
/* Tells the Alert object we'd like to not be highlighted anymore. */
{
    emit needHighlighting(false);
}


void AlertChoice::mouseReleaseEvent(QMouseEvent *)
/* We were clicked. */
{
    emit clicked();
}


void AlertChoice::mouseMoveEvent(QMouseEvent *)
/* Sometimes enterEvent's don't get triggered if the cursor moves too fast,
 * or it's possible another choice was highlighted with keyboard while the mouse
 * cursor was still over this one. So we request highlighting on mouseMove. */
{
    emit needHighlighting(true);
}




Alert::Alert(const QString &topText, const QString &subText,
             const AlertButton defaultHovered, QWidget *parent)
    : QWidget(parent)
{
    // Grab I/O focus
    grabKeyboard();
    this->setMouseTracking(true);

    m_pen = QPen(this->palette().dark(), 7);

    // Setup contents
    QVBoxLayout *layout = new QVBoxLayout();

    BloomedLabel *toplabel = new BloomedLabel();
    toplabel->setAlignment(Qt::AlignCenter);
    toplabel->setText(topText);
    BloomedLabel *sublabel = 0;
    if (! subText.isEmpty()) {
        sublabel = new BloomedLabel();
        sublabel->setAlignment(Qt::AlignCenter);
        sublabel->setText(subText);
    }

    QHBoxLayout *buttons = new QHBoxLayout();
    yesbutt = new AlertChoice(tr("Yes"), (defaultHovered == AlertButton::Yes));
    QObject::connect(yesbutt, SIGNAL(clicked()), this, SLOT(onYesClicked()));
    QObject::connect(yesbutt, SIGNAL(needHighlighting(bool)),
                     this, SLOT(onYesEnteredOrLeft(bool)));
    nobutt = new AlertChoice(tr("No"), (defaultHovered == AlertButton::No));
    QObject::connect(nobutt, SIGNAL(clicked()), this, SLOT(onNoClicked()));
    QObject::connect(nobutt, SIGNAL(needHighlighting(bool)),
                     this, SLOT(onNoEnteredOrLeft(bool)));

    // Squish the buttons well in the middle
    buttons->addStretch(2);
    buttons->addWidget(yesbutt, 1);
    buttons->addWidget(nobutt, 1);
    buttons->addStretch(2);

    layout->addWidget(toplabel, 1);
    if (sublabel) {
        layout->addWidget(sublabel, 1);
    }
    layout->addLayout(buttons, 2);
    layout->setMargin(ALERTMARGIN);
    this->setLayout(layout);

    /* Resize.
     * Width: maxwidth of text + margin left and right.
     * Height: text + margin top + margin between text and buttons + margin
     * bottom. */
    int h = QWidget::fontMetrics().height();
    h *= sublabel ? 5 : 2;
    int w = qMax(QWidget::fontMetrics().width(topText),
                 QWidget::fontMetrics().width(subText));
    w += 2*ALERTMARGIN;
    h += 3*ALERTMARGIN;
    this->resize(w, h);

}


Alert::Alert(const QString &topText, const AlertButton defaultHovered,
      QWidget *parent)
    : Alert(topText, QString(), defaultHovered, parent)
{
    // Overload
}


void Alert::onYesClicked()
{
    emit yesClicked();
}


void Alert::onNoClicked()
{
    emit noClicked();
}


void Alert::onYesEnteredOrLeft(bool val)
{
    if (val) {
        // Entered
        yesbutt->setHighlighted(true);
        nobutt->setHighlighted(false);
    } else {
        // Left
        yesbutt->setHighlighted(false);
    }
    update();
}


void Alert::onNoEnteredOrLeft(bool val)
{
    if (val) {
        // Entered
        nobutt->setHighlighted(true);
        yesbutt->setHighlighted(false);
    } else {
        // Left
        nobutt->setHighlighted(false);
    }
    update();
}

void Alert::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.setPen(m_pen);
    painter.setBrush(this->palette().window());
    painter.setOpacity(0.9);
    // Curse you and your geometry conventions, Qt!
    painter.drawRect(0,0, this->width()-1, this->height()-1);
}


void Alert::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case (Qt::Key_Left):
        // Hover on yes
        nobutt->setHighlighted(false);
        yesbutt->setHighlighted(true);
        update();
        break;
    case (Qt::Key_Right):
        // Hover on no
        nobutt->setHighlighted(true);
        yesbutt->setHighlighted(false);
        update();
        break;
    default:
        // No-op
        ;
    }
}

void Alert::keyReleaseEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case (Qt::Key_Return):
    case (Qt::Key_Enter):
    case (Qt::Key_Space):
        // Do action from highlighted button
        if (yesbutt->isHighlighted()) {
            emit yesClicked();
        } else {
            emit noClicked();
        }
        break;
    case (Qt::Key_Escape):
        // Treat it as a no
        emit noClicked();
        break;
    default:
        // No-op
        ;
    }
}
