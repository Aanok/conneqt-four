#-------------------------------------------------
#
# Project created by QtCreator 2015-12-26T11:10:18
#
#-------------------------------------------------

# APPLICATION GUID: 0ecdef4c-aa9b-4020-9522-aeeb276c07ca

QT       += core gui opengl multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ConneQt-Four
TEMPLATE = app


SOURCES += main.cpp \
    gameengine.cpp \
    utils.cpp \
    gui.cpp \
    fallingtoken.cpp \
    boardgui.cpp \
    debug.cpp \
    mainmenu.cpp \
    alert.cpp \
    menuitems.cpp

HEADERS  += gameengine.h \
    utils.h \
    gui.h \
    fallingtoken.h \
    boardgui.h \
    debug.h \
    mainmenu.h \
    alert.h \
    menuitems.h

QMAKE_CXXFLAGS += -std=c++0x

RESOURCES += \
    resources.qrc
