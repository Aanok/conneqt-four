#include "boardgui.h"

BoardGUI::BoardGUI(const bool shaking, const bool postprocessing, GameEngine *game, GUI *parent)
    : QOpenGLWidget(parent)
{
    // Grab I/O
    setMouseTracking(true);
    grabKeyboard();

    // Setup GameEngine
    this->m_game = game;
    connect(this, SIGNAL(CommandCPUMove()), game, SLOT(OnCommandCPUMove()));
    connect(game, SIGNAL(MadeCPUMove(int, int)), this, SLOT(OnMadeCPUMove(int, int)));

    // Init private members
    m_hoverrow = m_hovercol = -1;
    m_guistate = STATE_READY;
    m_fallingtokcpu = m_fallingtokp1 = 0;
    m_fallentokens = 0;
    m_shakingAnim = 0;
    m_shakingenabled = shaking;
    m_ppenabled = postprocessing;
    m_vhsrenderer = 0;
    m_blurrenderer = 0;
    m_mainrenderer = 0;
    m_texboard = 0;
    m_textokcpu = 0;
    m_textokhover = 0;
    m_textokp1 = 0;
    m_ebo = QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);

    // Setup token drop SFX
    m_fallsfxp1.setSource(QUrl::fromLocalFile(QFileInfo("sound/shutter.wav").absoluteFilePath()));
    m_fallsfxcpu.setSource(QUrl::fromLocalFile(QFileInfo("sound/shutter.wav").absoluteFilePath()));

    // No minimum size set because GUI takes care of that
    resize(COLS*CELLSIZE, ROWS*CELLSIZE);
}


void BoardGUI::reset()
{
    // Kill animations (shaking is brief enough we don't have to bother)
    if (m_fallingtokp1) {
        delete m_fallingtokp1;
        m_fallingtokp1 = 0;
    }
    if (m_fallingtokcpu) {
        delete m_fallingtokcpu;
        m_fallingtokcpu = 0;
    }
    m_fallentokens = 0;

    // Reset ghost token and state machine
    m_hoverrow = m_hovercol = -1;
    m_guistate = STATE_READY;
}


void BoardGUI::enableShaking(const bool value)
{
    m_shakingenabled = value;
}


void BoardGUI::enablePostprocessing(const bool value)
{
    m_ppenabled = value;
}


void BoardGUI::setSFXVolume(const qreal vol)
{
    m_fallsfxcpu.setVolume(vol);
    m_fallsfxp1.setVolume(vol);
}

void BoardGUI::genTokSprite(const QColor color, QOpenGLTexture *&outTex)
/* Generates a sprite for a token of colour color and binds it to outTex.
 * REQUIRES: OpenGL context current, otherwise no binding can occur. */
{
    QImage token(SPRITESIZE, SPRITESIZE, QImage::Format_ARGB32_Premultiplied);
    QPainter p(&token);
    p.setRenderHint(QPainter::Antialiasing);
    p.setPen(color.darker(200)); // Halve V HSV value for border
    p.setBrush(color);
    token.fill(Qt::transparent);
    p.drawEllipse(SPRITESIZE*0.05, SPRITESIZE*0.05, SPRITESIZE*0.9, SPRITESIZE*0.9);
    outTex = new QOpenGLTexture(token);
    outTex->setMinMagFilters(QOpenGLTexture::LinearMipMapLinear,
                             QOpenGLTexture::LinearMipMapLinear);
    outTex->generateMipMaps();
}


void BoardGUI::initializeGL()
/* Automatically invoked before the first paintEvent, initializes the OpenGL
 * environment and variables. */
{
    // Create context & expose OpenGL functions
    initializeOpenGLFunctions();

    // Set clear color
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

    // Generate sprites for tokens
    genTokSprite(QColor(255,30,30), m_textokp1); // TOKP1
    genTokSprite(QColor(255,30,30), m_textokp1); // TOKP1 repeated to hotfix a bug
    genTokSprite(QColor(255,255,30), m_textokcpu); // TOKCPU
    genTokSprite(QColor(255,127,127), m_textokhover); // hovering token

    // Generate sprite for the board layer (blue swiss cheese pattern)
    QImage boardlayer(COLS*SPRITESIZE, ROWS*SPRITESIZE, QImage::Format_ARGB32);
    boardlayer.fill(QColor(30,30,255)); // boardcolor
    QPainter boardpainter(&boardlayer);
    boardpainter.setCompositionMode(QPainter::CompositionMode_SourceOut);
    boardpainter.setRenderHint(QPainter::Antialiasing);
    boardpainter.setPen(Qt::NoPen);
    boardpainter.setBrush(QColor(255,255,255));
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            boardpainter.drawEllipse(SPRITESIZE*0.05, SPRITESIZE*0.05, SPRITESIZE*0.9, SPRITESIZE*0.9);
            boardpainter.translate(SPRITESIZE, 0);
        }
        boardpainter.translate(-COLS*SPRITESIZE, SPRITESIZE);
    }
    m_texboard = new QOpenGLTexture(boardlayer);
    m_texboard->setMinMagFilters(QOpenGLTexture::Linear, QOpenGLTexture::Linear);
    m_texboard->generateMipMaps();

    // Instantiate MAIN RENDERER with shaders
    m_mainrenderer = new QOpenGLShaderProgram();
    m_mainrenderer->addShaderFromSourceFile(
                QOpenGLShader::Vertex, ":/shaders/transformingVertexShader.src");
    m_mainrenderer->addShaderFromSourceFile(
                QOpenGLShader::Fragment, ":/shaders/neutralFragmentShader.src");
    m_mainrenderer->link();

    m_mainrenderer->bind();

    // Vertex array object
    m_vao.create();
    m_vao.bind();

    // Vertex buffer object
    m_vbo.create();
    m_vbo.bind();

    GLfloat vertices[] = { // A rectangle
    //  Vertex      Texture
        -1.f,  1.f, 0.f, 0.f, // Top-left
         1.f,  1.f, 1.f, 0.f, // Top-right
         1.f, -1.f, 1.f, 1.f, // Bottom-right
        -1.f, -1.f, 0.f, 1.f, // Bottom-left
    };

    m_vbo.allocate(vertices, sizeof(vertices));

    // Specify the layout of the vertex data for main renderer
    int posAttrib = m_mainrenderer->attributeLocation("position");
    glEnableVertexAttribArray(posAttrib);
    glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 4*sizeof(float), 0);
    int texAttrib = m_mainrenderer->attributeLocation("texcoord");
    glEnableVertexAttribArray(texAttrib);
    glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE,
                          4*sizeof(float), (void*)(2*sizeof(float)));

    // Element buffer
    m_ebo.create();
    m_ebo.bind();

    GLuint elements[] = { // To draw the rectangle in question
        0, 1, 2,
        0, 2, 3
    };

    m_ebo.allocate(elements, sizeof(elements));

    // Uniforms
    QMatrix4x4 projection;
    projection.ortho(0.f, COLS*CELLSIZE, ROWS*CELLSIZE, 0.f, -1.f, 1.f);
    m_mainrenderer->setUniformValue("projection", projection);

    m_mainrenderer->release();

    // Instantiate BLUR RENDERER with shaders
    m_blurrenderer = new QOpenGLShaderProgram();
    m_blurrenderer->addShaderFromSourceFile(
                QOpenGLShader::Vertex, ":/shaders/neutralVertexShader.src");
    m_blurrenderer->addShaderFromSourceFile(
                QOpenGLShader::Fragment, ":/shaders/gaussianBlurFragmentShader.src");
    m_blurrenderer->link();

    // Specify the layout of the vertex data for blur renderer
    posAttrib = m_blurrenderer->attributeLocation("position");
    glEnableVertexAttribArray(posAttrib);
    glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 4*sizeof(float), 0);
    texAttrib = m_blurrenderer->attributeLocation("texcoord");
    glEnableVertexAttribArray(texAttrib);
    glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE,
                          4*sizeof(float), (void*)(2*sizeof(float)));

    m_blurrenderer->bind();

    // Uniforms
    m_blurrenderer->setUniformValue("texSize", this->size());

    m_blurrenderer->release();


    // Instantiate VHS RENDERER, with shaders
    m_vhsrenderer = new QOpenGLShaderProgram();
    m_vhsrenderer->addShaderFromSourceFile(
                QOpenGLShader::Vertex, ":/shaders/neutralVertexShader.src");
    m_vhsrenderer->addShaderFromSourceFile(
                QOpenGLShader::Fragment, ":/shaders/VHSFragmentShader.src");
    m_vhsrenderer->link();

    // Specify the layout of the vertex data for vhs renderer
    posAttrib = m_vhsrenderer->attributeLocation("position");
    glEnableVertexAttribArray(posAttrib);
    glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 4*sizeof(float), 0);
    texAttrib = m_vhsrenderer->attributeLocation("texcoord");
    glEnableVertexAttribArray(texAttrib);
    glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE,
                          4*sizeof(float), (void*)(2*sizeof(float)));

    m_vhsrenderer->bind();
    m_vhsrenderer->release();

    // FRAMEBUFFERS
    setupFBO();
}


void BoardGUI::paintGL()
{
    // MAIN RENDER PASS
    m_mainrenderer->bind();
    if (m_ppenabled) {
        glBindFramebuffer(GL_FRAMEBUFFER, m_fbo[0]);
    }
    // Clear background
    glClear(GL_COLOR_BUFFER_BIT);
    // Enable blending
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    // Draw static P1 tokens
    m_textokp1->bind();
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            if (m_game->GetTok(i,j) == TOKP1 &&
                    (!m_fallingtokp1 || i != m_fallingtokp1->row || j != m_fallingtokp1->col)) {
                // There's a TOKP1 that's not falling here
                drawBoundSprite(QRectF(j*CELLSIZE, i*CELLSIZE, CELLSIZE, CELLSIZE));
            }
        }
    }
    // Draw falling player token, if any
    if (m_fallingtokp1) {
        drawBoundSprite(QRectF(m_fallingtokp1->col*CELLSIZE, m_fallingtokp1->y(),CELLSIZE, CELLSIZE));
    }
    m_textokp1->release();
    // Draw static CPU tokens
    m_textokcpu->bind();
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            if (m_game->GetTok(i,j) == TOKP2 &&
                    (!m_fallingtokcpu || i != m_fallingtokcpu->row || j != m_fallingtokcpu->col)) {
                // There's a TOKP2 that's not falling here
                drawBoundSprite(QRectF(j*CELLSIZE, i*CELLSIZE, CELLSIZE, CELLSIZE));
            }
        }
    }
    // Draw falling CPU token, if any
    if (m_fallingtokcpu) {
        drawBoundSprite(QRectF(m_fallingtokcpu->col*CELLSIZE, m_fallingtokcpu->y(), CELLSIZE, CELLSIZE));
    }
    m_textokcpu->release();
    // Draw hover token
    if (m_guistate != STATE_BUSY) {
        m_textokhover->bind();
        drawBoundSprite(QRectF(m_hovercol*CELLSIZE, m_hoverrow*CELLSIZE, CELLSIZE, CELLSIZE));
        m_textokhover->release();
    }
    // Draw board overlay
    m_texboard->bind();
    drawBoundSprite(QRectF(0,0, COLS*CELLSIZE, ROWS*CELLSIZE));
    m_texboard->release();
    // Disable blending
    glDisable(GL_BLEND);
    m_mainrenderer->release();

    // POSTPROCESSING PASS
    if (m_ppenabled) {
        // Horizontal gaussian blur
        m_blurrenderer->bind();
        glBindFramebuffer(GL_FRAMEBUFFER, m_fbo[1]);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_fbotex[0]);
        m_blurrenderer->setUniformValue("dir", QVector2D(1.0, 0.0));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

        // Vertical gaussian blur
        glBindFramebuffer(GL_FRAMEBUFFER, m_fbo[2]);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_fbotex[1]);
        m_blurrenderer->setUniformValue("dir", QVector2D(0.0, 1.0));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
        m_blurrenderer->release();

        // VHS effect, final pass
        m_vhsrenderer->bind();
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_fbotex[2]);
        m_vhsrenderer->setUniformValue("time", (GLfloat)(QTime::currentTime().msec()*0.1));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
        m_vhsrenderer->release();
    }
}


void BoardGUI::drawBoundSprite(const QRectF dest)
/* Draws the currently bound texture to the worldspace coordinates rectangle dest.
 * REQUIRES: shader program bound, texture bound, EBO bound. */
{
    QMatrix4x4 model;
    model.translate(dest.left()+dest.width()*.5f, dest.top()+dest.height()*.5f);
    model.scale(dest.width()*.5f, dest.height()*.5f);
    m_mainrenderer->setUniformValue("model", model);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}


void BoardGUI::setupFBO()
{
    // Delete previous stuff --don't worry about first time invocation, it won't do any harm.
    glDeleteTextures(3, m_fbotex);
    glDeleteFramebuffers(3, m_fbo);

    // Create framebuffers
    glGenFramebuffers(3, m_fbo);

    // Create framebuffer textures
    glGenTextures(3, m_fbotex);

    // Configure textures for the FBOs' colour buffers
    for (int i = 0; i < 3; i++) {
        glBindFramebuffer(GL_FRAMEBUFFER, m_fbo[i]);

        glBindTexture(GL_TEXTURE_2D, m_fbotex[i]);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, this->width(), this->height(),
                     0, GL_RGB, GL_UNSIGNED_BYTE, NULL);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
                               m_fbotex[i], 0);
    }
}


void BoardGUI::resizeEvent(QResizeEvent *e)
/* Some uniforms depend on the viewspace dimensions so we upadte them. */
{
    QOpenGLWidget::resizeEvent(e);
    if (m_vhsrenderer) {
        // We want the scanlines to appear every four real pixels on the monitor.
        m_vhsrenderer->bind();
        m_vhsrenderer->setUniformValue("texheight", (GLfloat)(e->size().height()));
        m_vhsrenderer->release();
    }
    if (m_blurrenderer) {
        // FBO's are as big as the widget's viewspace size.
        m_blurrenderer->bind();
        setupFBO();
        m_blurrenderer->setUniformValue("texSize", this->size());
        m_blurrenderer->release();
    }
}


void BoardGUI::mouseMoveEvent(QMouseEvent *event)
/* On mouseMove we update the ghost token, if it's P1's turn. */
{
    if (m_guistate == STATE_READY) {
        m_hovercol = event->pos().x()*COLS/this->width();
        m_hoverrow = m_game->GetFirstFreeRow(m_hovercol);
    }
}


void BoardGUI::mousePressEvent(QMouseEvent *event)
/* On mousePress we bind where the next TOKP1 will fall, if it's P1's turn. */
{
    if (m_guistate == STATE_READY) {
        m_hovercol = event->pos().x()*COLS/this->width();
        m_hoverrow = m_game->GetFirstFreeRow(m_hovercol);
        m_guistate = STATE_ADDING;
    }
}


void BoardGUI::mouseReleaseEvent(QMouseEvent *)
/* On mouseRelease we add a TOKP1, if we had bound one before. */
{
    if (m_guistate == STATE_ADDING) {
        addToken();
    }
}


void BoardGUI::keyPressEvent(QKeyEvent *event)
/* Keyboard presses allow the use to move the ghost or binding a TOKP1 to drop. */
{
    switch (event->key()) {
    case (Qt::Key_Left):
        // Move hovercol to the next empty column to the left (cylindrically)
        do {
            m_hovercol = (COLS+m_hovercol-1) % COLS; // +COLS because negatives and modulus are fun
            m_hoverrow = m_game->GetFirstFreeRow(m_hovercol);
        } while (m_hoverrow == -1);
        break;
    case (Qt::Key_Right):
        // Move hovercol to the next empty column to the right (cylindrically)
        do {
            m_hovercol = (m_hovercol+1) & COLS;
            m_hoverrow = m_game->GetFirstFreeRow(m_hovercol);
        } while (m_hoverrow == -1);
        break;
    case (Qt::Key_Return): // Big Enter key
    case (Qt::Key_Enter): // Numpad Enter key
    case (Qt::Key_Space):
        // Start placing token at current hovercol
        if (m_guistate == STATE_READY) {
            m_guistate = STATE_ADDING;
        }
        break;
    default:
        // No-op
        ;
    }
}


void BoardGUI::keyReleaseEvent(QKeyEvent *event)
/* Key releases make a bound TOKP1 fall or allow the user to quit the application. */
{
    switch (event->key()) {
    case (Qt::Key_Return):
    case (Qt::Key_Enter):
    case (Qt::Key_Space):
        // Place token at current hovercol
        if (m_guistate == STATE_ADDING) {
            addToken();
        }
        break;
    case (Qt::Key_Escape):
        // Tell GUI the user would like to quit
        ((GUI*)(parent()))->onQuitRequest();
    default:
        // No-op
        ;
    }
}


void BoardGUI::addToken()
/* Makes a TOKP1 drop to ghost coordinates.
 * REQUIRES: state machine in STATE_ADDING, P1's turn. */
{
    m_game->MakeP1Move(m_hoverrow, m_hovercol);
    /* We check again as the game could have ended meanwhile!
     * The signal is caught by GUI. */
    if (m_guistate == STATE_ADDING) {
        // The token falls along a parabola so the drop time is O(sqrt(row))
        m_fallingtokp1 = new FallingToken(m_hovercol*CELLSIZE, -CELLSIZE,
                                          m_hoverrow*CELLSIZE, std::sqrt((m_hoverrow+1)*CELLTRAVERSETIME));
        connect(m_fallingtokp1->animation, SIGNAL(finished()), this, SLOT(OnFallingTokP1Finished()));
        m_fallingtokp1->animation->start();
        m_guistate = STATE_BUSY;
        emit CommandCPUMove();
    }
}


void BoardGUI::initShakingAnimation()
/* Makes the board randomly shake for SHAKINGTIME milliseconds. */
{
    m_shakingAnim = new QPropertyAnimation(this, "pos");
    connect(m_shakingAnim, SIGNAL(finished()), this, SLOT(onShakingFinished()));
    m_shakingAnim->setDuration(SHAKINGTIME);
    int s1, s2, x, y;
    x = this->pos().x();
    y = this->pos().y();
    for (qreal i = 1; i < 10; i++) {
        s1 = qrand()%2 ? -1 : 1;
        s2 = qrand()%2 ? -1 : 1;
        // 15px of displacement
        m_shakingAnim->setKeyValueAt(i*0.1, QPoint(x+s1*qrand()%15, y+s2*qrand()%15));
    }
    m_shakingAnim->setStartValue(this->pos());
    m_shakingAnim->setEndValue(this->pos());
    m_shakingAnim->start();
}


void BoardGUI::OnMadeCPUMove(int row, int col)
/* Slot to animate a TOKP2 falling down. */
{
    m_hoverrow = m_game->GetFirstFreeRow(m_hovercol);
    m_fallingtokcpu = new FallingToken(col*CELLSIZE, -CELLSIZE,
                                       row*CELLSIZE, std::sqrt((row+1)*CELLTRAVERSETIME));
    connect(m_fallingtokcpu->animation, SIGNAL(finished()), this, SLOT(OnFallingTokCPUFinished()));
    if (! m_fallingtokp1) {
        // There is no TOKP1 falling that we have to wait for
        m_fallingtokcpu->animation->start();
    }
}


void BoardGUI::OnFallingTokP1Finished()
/* Slot invoked at end of TOKP1 falling animation. Cleans up, starts or resets
 * the shaking, starts the animation of TOKP2 if it was waiting.
 * Transitions to STATE_READY if it's P1's turn again. */
{
    if (m_fallingtokp1) {
        delete m_fallingtokp1;
        m_fallingtokp1 = 0;
        m_fallsfxp1.play();
        if (m_fallingtokcpu) {
            // Theres a TOKP2 waiting to fall
            m_fallingtokcpu->animation->start();
        }
        // N.B. we don't know if TOKP2 has fallen yet or if the AI is still computing!
        if (m_shakingenabled) {
            if (m_shakingAnim) {
                m_shakingAnim->stop();
                this->move(m_shakingAnim->endValue().toPoint());
                delete m_shakingAnim;
            }
            initShakingAnimation();
        }
        if (++m_fallentokens >= 2) {
            m_fallentokens = 0;
            m_guistate = STATE_READY;
        }
    }
}


void BoardGUI::OnFallingTokCPUFinished()
/* Same as above, but for the end of TOKP2's animation. */
{
    if (m_fallingtokcpu) {
        delete m_fallingtokcpu;
        m_fallingtokcpu = 0;
        m_fallsfxcpu.play();
        if (m_shakingenabled) {
            if (m_shakingAnim) {
                m_shakingAnim->stop();
                this->move(m_shakingAnim->endValue().toPoint());
                delete m_shakingAnim;
            }
            initShakingAnimation();
        }
        if (++m_fallentokens >= 2) {
            m_fallentokens = 0;
            m_guistate = STATE_READY;
        }
    }
}


void BoardGUI::onShakingFinished()
/* Clean up at the end of the shaking animation. */
{
    if (m_shakingAnim) {
        delete m_shakingAnim;
        m_shakingAnim = 0;
    }
}

void BoardGUI::OnTimeout()
/* Things to do when the main timer triggers an update. */
{
    // Queue paint event
    update();
}
