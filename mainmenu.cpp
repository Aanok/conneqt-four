#include "mainmenu.h"

MainMenu::MainMenu(GUI *parent, QSize size)
    : QWidget(parent)
{
    // Setup I/O and size
    setMouseTracking(true);
    setMinimumSize(size);
    resize(size);

    // Setup animation helpers
    m_opened = false;
    m_animation = 0;

    // Setup shapes for accents
    m_arrow << QPoint(HANDLEWIDTH*0.3, 0)
          << QPoint(HANDLEWIDTH*0.7, -HANDLEWIDTH)
          << QPoint(HANDLEWIDTH*0.7, HANDLEWIDTH);

    // Setup up music
    musicplayer = new QMediaPlayer(this);
    QMediaPlaylist *playlist = new QMediaPlaylist(musicplayer);
    playlist->addMedia(QUrl::fromLocalFile(QFileInfo("sound/HOME - Oort Cloud.mp3").absoluteFilePath()));
    playlist->addMedia(QUrl::fromLocalFile(QFileInfo("sound/HOME - Tides.mp3").absoluteFilePath()));
    playlist->addMedia(QUrl::fromLocalFile(QFileInfo("sound/HOME - Resonance.mp3").absoluteFilePath()));
    playlist->addMedia(QUrl::fromLocalFile(QFileInfo("sound/HOME - Come Back Down.mp3").absoluteFilePath()));
    playlist->addMedia(QUrl::fromLocalFile(QFileInfo("sound/HOME - Half Moon.mp3").absoluteFilePath()));
    playlist->setPlaybackMode(QMediaPlaylist::Loop);
    playlist->setCurrentIndex(0);
    musicplayer->setPlaylist(playlist);
    musicplayer->setVolume(Utils::appSettings.musicVolume);
    musicplayer->play();


    // Setup menu contents
    QVBoxLayout *upperlayout = new QVBoxLayout();

    // Maxplies control
    BloomedLabel *maxplieslabel = new BloomedLabel(tr("Difficulty"));
    maxplieslabel->setContentsMargins(MARGIN,0,0,0);
    ValueSlideshow *maxplies = new ValueSlideshow();
    maxplies->addValue(10);
    maxplies->setLabel(10, "Easy");
    maxplies->addValue(11);
    maxplies->setLabel(11, "Medium");
    maxplies->addValue(12);
    maxplies->setLabel(12, "Hard");
    maxplies->setCurrentValue(Utils::appSettings.difficulty); // Default
    QHBoxLayout *maxplieslayout = new QHBoxLayout();
    maxplieslayout->addWidget(maxplieslabel);
    maxplieslayout->setAlignment(maxplieslabel, Qt::AlignLeft);
    maxplieslayout->addWidget(maxplies);
    maxplieslayout->setAlignment(maxplies, Qt::AlignRight);
    connect(maxplies, SIGNAL(valueChanged(int)), parent, SLOT(onMaxpliesChanged(int)));

    // FPS control
    ValueSlideshow *maxfps = new ValueSlideshow();
    maxfps->addValue(24);
    maxfps->addValue(30);
    maxfps->addValue(50);
    maxfps->addValue(60);
    maxfps->addValue(72);
    maxfps->addValue(120);
    maxfps->setCurrentValue(Utils::appSettings.maxFPS);
    connect(maxfps, SIGNAL(valueChanged(int)), parent, SLOT(onMaxFPSChanged(int)));
    BloomedLabel *maxfpslabel = new BloomedLabel(tr("Max FPS"));
    QHBoxLayout *maxfpslayout = new QHBoxLayout();
    maxfpslabel->setContentsMargins(MARGIN,0,0,0);
    maxfpslayout->addWidget(maxfpslabel);
    maxfpslayout->setAlignment(maxfpslabel, Qt::AlignLeft);
    maxfpslayout->addWidget(maxfps);
    maxfpslayout->setAlignment(maxfps, Qt::AlignRight);

    // Fullscreen control
    CheckBox *fullscreen = new CheckBox(tr("Fullscreen"), Utils::appSettings.fullscreen);
    connect(fullscreen, SIGNAL(stateChanged(int)), parent, SLOT(onFullscreenChange(int)));

    // Shaking control
    CheckBox *shaking = new CheckBox(tr("Shake on drop"), Utils::appSettings.shaking);
    connect(shaking, SIGNAL(stateChanged(int)), parent, SLOT(onShakingChanged(int)));

    // Postprocessing control
    CheckBox *postprocessing = new CheckBox(tr("Hotline ConneQt"), Utils::appSettings.postProcessing);
    connect(postprocessing, SIGNAL(stateChanged(int)), parent, SLOT(onPostprocessingChanged(int)));

    // Separator
    QFrame *line1 = new QFrame();
    line1->setFrameShape(QFrame::HLine);
    line1->setFrameShadow(QFrame::Sunken);

    // Audio section
    BloomedLabel *audiosection = new BloomedLabel(tr("Audio controls"));

    // Reproduction controls
    MenuButton *skiptrack = new MenuButton(tr("Skip"));
    connect(skiptrack, SIGNAL(clicked()), musicplayer->playlist(), SLOT(next()));
    MenuButton *pausemusic = new MenuButton(tr("Pause"));
    connect(pausemusic, SIGNAL(clicked()), musicplayer, SLOT(pause()));
    MenuButton *resumemusic = new MenuButton(tr("Play"));
    connect(resumemusic, SIGNAL(clicked()), musicplayer, SLOT(play()));
    QHBoxLayout *musiclayout = new QHBoxLayout();
    musiclayout->addWidget(skiptrack);
    musiclayout->addWidget(pausemusic);
    musiclayout->addWidget(resumemusic);

    // Music volume control
    BloomedLabel *musicvolumelabel = new BloomedLabel(tr("Music"));
    Slider *musicvolume = new Slider(Utils::appSettings.musicVolume, 0, 100);
    connect(musicvolume, SIGNAL(valueChanged(int)), this, SLOT(onMusicVolumeChanged(int)));
    QHBoxLayout *musicvolumelayout = new QHBoxLayout();
    musicvolumelayout->addWidget(musicvolumelabel);
    musicvolumelayout->addWidget(musicvolume);

    // SFX volume control
    BloomedLabel *sfxvolumelabel = new BloomedLabel(tr("SFX"));
    Slider *sfxvolume = new Slider(Utils::appSettings.sfxVolume, 0, 100);
    connect(sfxvolume, SIGNAL(valueChanged(int)), parent, SLOT(onSFXVolumeChanged(int)));
    QHBoxLayout *sfxvolumelayout = new QHBoxLayout();
    sfxvolumelayout->addWidget(sfxvolumelabel);
    sfxvolumelayout->addWidget(sfxvolume);

    // Now playing
    BloomedLabel *nowplaying = new BloomedLabel(tr("Now Playing:"));
    currenttrack = new BloomedLabel(getCurrentTrackName());
    connect(musicplayer, SIGNAL(currentMediaChanged(QMediaContent)), this, SLOT(onTrackChanged()));

    // Separator
    QFrame *line2 = new QFrame();
    line2->setFrameShape(QFrame::HLine);
    line2->setFrameShadow(QFrame::Sunken);

    // Quit button
    MenuButton *quitbutton = new MenuButton(tr("Quit"));
    quitbutton->setMinimumSize(200, 50);
    connect(quitbutton, SIGNAL(clicked()), parent, SLOT(onQuitRequest()));
    connect(quitbutton, SIGNAL(clicked()), this, SLOT(close()));

    // Now to assemble the layout, oh joy.
    upperlayout->setContentsMargins(HANDLEWIDTH+MARGIN,MARGIN,MARGIN,MARGIN);
    upperlayout->addLayout(maxplieslayout, 1);
    upperlayout->setAlignment(maxplieslayout, Qt::AlignVCenter);
    upperlayout->addLayout(maxfpslayout, 1);
    upperlayout->setAlignment(maxfpslayout, Qt::AlignVCenter);
    upperlayout->addWidget(fullscreen, 1);
    upperlayout->setAlignment(fullscreen, Qt::AlignVCenter);
    upperlayout->addWidget(shaking, 1);
    upperlayout->setAlignment(fullscreen, Qt::AlignVCenter);
    upperlayout->addWidget(postprocessing, 1);
    upperlayout->setAlignment(fullscreen, Qt::AlignVCenter);
    upperlayout->addWidget(line1);
    upperlayout->addWidget(audiosection, 1);
    upperlayout->setAlignment(audiosection, Qt::AlignCenter);
    upperlayout->addLayout(musiclayout, 1);
    upperlayout->addLayout(musicvolumelayout, 1);
    upperlayout->addLayout(sfxvolumelayout, 1);
    upperlayout->addStretch(1);
    upperlayout->addWidget(nowplaying);
    upperlayout->setAlignment(nowplaying, Qt::AlignCenter);
    upperlayout->addWidget(currenttrack);
    upperlayout->setAlignment(currenttrack, Qt::AlignCenter);
    upperlayout->addWidget(line2);
    upperlayout->addWidget(quitbutton, 10);
    upperlayout->setAlignment(quitbutton, Qt::AlignCenter);

    this->setLayout(upperlayout);
}


QString MainMenu::getCurrentTrackName()
/* Parses the name (author + title) of the track currently playing from its
 * filename. I tried using metadata but could not get it to work. */
{
    return Utils::cropFilenameExtension(musicplayer->currentMedia().canonicalUrl().fileName());
}


void MainMenu::open()
/* Starts the animation to open the menu and sets m_opened to true. */
{
    m_animation = new QPropertyAnimation(this, "pos");
    m_animation->setEndValue(this->pos()-QPoint(this->width()-HANDLEWIDTH, 0));
    m_animation->setDuration(ANIMTIME);
    m_animation->setEasingCurve(QEasingCurve::OutExpo);
    connect(m_animation, SIGNAL(finished()), this, SLOT(OnFinishedAnimation()));
    m_animation->start();
    m_opened = true;
}


void MainMenu::close()
/* Starts the animation to close the menu and sets m_opened to false. */
{
    m_animation = new QPropertyAnimation(this, "pos");
    m_animation->setEndValue(this->pos()+QPoint(this->width()-HANDLEWIDTH, 0));
    m_animation->setDuration(ANIMTIME);
    m_animation->setEasingCurve(QEasingCurve::OutExpo);
    connect(m_animation, SIGNAL(finished()), this, SLOT(OnFinishedAnimation()));
    m_animation->start();
    m_opened = false;
}


bool MainMenu::isOpened() const
/* Getter for "opened" property. */
{
    return m_opened;
}


void MainMenu::processWindowStateChange()
/* Invoked by GUI when there's a change in the window's state. */
{
    /* The window was maximized, minimized or toggled fullscreen.
     * A running animation would be broken by the event, so we adjust it manually,
     * if need be. */
    if (m_animation) {
        m_animation->stop();
        delete m_animation;
        m_animation = 0;
        // The following breaks encapsulation. We'll have to live with the shame, I guess.
        if (m_opened) {
            // It was an opening animation.
            this->move(((GUI*)this->parent())->width() - this->width(), 0);
        } else {
            // It was a closing animation.
            this->move(((GUI*)this->parent())->width() - HANDLEWIDTH, 0);
        }
    }
}


void MainMenu::OnFinishedAnimation()
/* Slot for the end of opening/closing animation. Does some cleanup. */
{
    delete m_animation;
    m_animation = 0;
}


void MainMenu::onTrackChanged()
/* Slot for changing track at the user's request. */
{
    currenttrack->setText(getCurrentTrackName());
}


void MainMenu::onMusicVolumeChanged(int value)
/* Slot for changing the music volume at the user's request. */
{
    // Apply (N.B. it's clamped automatically by QMediaPLayer)
    musicplayer->setVolume(value);

    // Update settings
    Utils::appSettings.musicVolume = value;
}


void MainMenu::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

    // Draw the background and the dark "handle" on the left.
    painter.fillRect(0,0, HANDLEWIDTH, this->height(), this->palette().dark());
    painter.fillRect(HANDLEWIDTH,0, this->width()-HANDLEWIDTH, this->height(), this->palette().window());

    /* Depending on the the cursor's position, the handle can have a brigther
     * arrow, facing outwards or invards. */
    int delta = -QWidget::mapFromGlobal(QCursor::pos()).x();
    if (this->isEnabled() && delta <= 150) {
        painter.setPen(Qt::NoPen);
        painter.setBrush(this->palette().light());
        painter.setOpacity(1-(qreal)delta/150.f);
        painter.translate(0, this->height()/2);
        if (m_opened) {
            painter.translate(HANDLEWIDTH,0);
            painter.scale(-1, 1);
        }
        painter.drawPolygon(m_arrow);
    }
}


void MainMenu::enterEvent(QEvent *)
/* On an enterEvent, if the menu is enabled, not already animating and closed,
 * we open it. */
{
    if (this->isEnabled() && !m_animation && !m_opened) {
        open();
    }
}


void MainMenu::leaveEvent(QEvent *)
/* On an leaveEvent, if the menu is enabled, not already animating and opened,
 * we close it. */
{
    if (this->isEnabled() && !m_animation && m_opened) {
        close();
    }
}
