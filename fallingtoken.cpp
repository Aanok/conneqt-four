#include "fallingtoken.h"

FallingToken::FallingToken(const int x, const int y,
                           const int endy, const int falltime)
    : QObject()
{
    m_x = x;
    m_y = y;
    row = endy/CELLSIZE;
    col = x/CELLSIZE;
    animation = new QPropertyAnimation(this, "y");
    animation->setDuration(falltime);
    animation->setEndValue(endy);
    // N.B. easing is a parabola!
    animation->setEasingCurve(QEasingCurve::InQuad);
}


int FallingToken::x() const
{
    return m_x;
}


void FallingToken::setX(const int x)
{
    m_x = x;
}


int FallingToken::y() const
{
    return m_y;
}


void FallingToken::setY(const int y)
{
    m_y = y;
}
