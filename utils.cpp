#include "utils.h"
#include <iostream>

// SETTINGS
Settings::Settings()
// Constructor with default values in case of file not found
{
    difficulty = 10; // Easy
    maxFPS = 60;
    fullscreen = false;
    shaking = true;
    postProcessing = true;
    maximized = false;
    musicVolume = 75;
    sfxVolume = 60;
    windowSize = QSize(700,600);
}


Settings::Settings(const QJsonObject &o)
// Constructor from Json object
{
    difficulty = o["difficulty"].toInt();
    maxFPS = o["maxFPS"].toInt();
    fullscreen = o["fullscreen"].toBool();
    shaking = o["shaking"].toBool();
    postProcessing = o["postProcessing"].toBool();
    maximized = o["maximized"].toBool();
    musicVolume = o["musicVolume"].toInt();
    sfxVolume = o["sfxVolume"].toInt();
    windowSize = QSize(o["width"].toInt(), o["height"].toInt());
}


QJsonObject Settings::toJson()
// Returns Json representation of instance
{
    QJsonObject o;

    o["difficulty"] = difficulty;
    o["maxFPS"] = maxFPS;
    o["fullscreen"] = fullscreen;
    o["shaking"] = shaking;
    o["postProcessing"] = postProcessing;
    o["maximized"] = maximized;
    o["musicVolume"] = musicVolume;
    o["sfxVolume"] = sfxVolume;
    o["width"] = windowSize.width();
    o["height"] = windowSize.height();

    return o;
}


// UTILS
// Redefining static members because LOL C++ LINKING AMIRITE?
Settings Utils::appSettings;


QGraphicsDropShadowEffect* Utils::addGlowToWidget(QWidget *target)
/* Adds a native Qt glow effect to target. It's quite heavy but w/e. */
{
    if (target) {
        QGraphicsDropShadowEffect *effect = new QGraphicsDropShadowEffect(target);
        effect->setBlurRadius(7.5);
        // We use a dark colour, which gives a subtle but nice effect.
        effect->setColor(target->palette().base().color());
        effect->setOffset(0,0);
        target->setGraphicsEffect(effect);
        return effect;
    } else {
        return 0;
    }
}


QString Utils::cropFilenameExtension(const QString filename)
/* Takes a string and returns its prefix up to the first period found. */
{
    return filename.split(".", QString::SkipEmptyParts).at(0);
}


void Utils::init()
/* Initializes the class, loading the settings. */
{
    readSettings();
}


void Utils::cleanup(GameEngine *ge)
/* Invoked on application quit, it commits the settings to file and halts the
 * GameEngine minimax thread to allow a graceful shutdown. */
{
    writeSettings();
    ge->QuitGame();
}


void Utils::readSettings()
/* Reads application settings from file. Loads defaults if non found. */
{
    appSettings = Settings();
    QFile settingsFile(QFileInfo("settings.json").absoluteFilePath());
    if (!settingsFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
        // Couldn't find or open file, will load defaults.
        appSettings = Settings();
    } else {
        QByteArray settingsData = settingsFile.readAll();
        QJsonDocument settingsDoc = QJsonDocument::fromJson(settingsData);
        appSettings = Settings(settingsDoc.object());
        settingsFile.close();
    }
}


void Utils::writeSettings()
/* Commits current application settings to file, if possible. */
{
    QFile settingsFile(QFileInfo("settings.json").absoluteFilePath());
    if (settingsFile.open(QIODevice::WriteOnly)) {
        // File opened correctly
        QJsonObject settingsJson = Utils::appSettings.toJson();
        QJsonDocument settingsDoc(settingsJson);
        settingsFile.write(settingsDoc.toJson());
        settingsFile.close();
    }
}
