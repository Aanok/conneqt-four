#include "gui.h"
#include "gameengine.h"
#include "utils.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // FONTS
    /* N.B. this font has the sizing wrong, it returns twice as much as it
     * shoud on all fontMetrics. */
    QFontDatabase::addApplicationFont(":/res/amiga4ever.ttf"); // 6pts
    QFont pixelatedfont = QFont("Amiga Forever");
    pixelatedfont.setPointSize(12);
    a.setFont(pixelatedfont);

    // COLOUR PALETTE
    QPalette p = a.palette();
    p.setColor(QPalette::Base, QColor(10,10,10));
    p.setColor(QPalette::Window, QColor(46, 46, 107));
    p.setColor(QPalette::Dark, QColor(8, 8, 69));
    p.setColor(QPalette::Light, QColor(253, 239, 206));
    p.setColor(QPalette::Highlight, QColor(253, 239, 206));
    p.setColor(QPalette::WindowText, QColor(252, 217, 136));
    p.setColor(QPalette::ButtonText, QColor(252, 217, 136));
    p.setColor(QPalette::Button, QColor(8, 8, 69));
    a.setPalette(p);

    // Init utils, then setup.
    Utils::init();
    GameEngine game;
    GUI w(NULL, &game);
    game.start();
    if (Utils::appSettings.fullscreen) {
        w.showFullScreen();
    } else if (Utils::appSettings.maximized) {
        w.showMaximized();
    } else {
        w.show();
    }
    w.focusWidget();

    // Launch
    return a.exec();
}
