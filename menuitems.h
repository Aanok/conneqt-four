#ifndef MENUITEMS_H
#define MENUITEMS_H

#include "utils.h"
#include "alert.h"
#include <QtWidgets>

#define ARROWBUTTONMAXWIDTH 25

/* Utility header populated with a number of custom controls used in MainMenu
 * and Alert. */

// SLIDER
class Slider : public QWidget
{
    Q_OBJECT

public:
    Slider(const int def, const int min, const int max, QWidget *parent = 0);

private:
    int m_value, m_minbound, m_maxbound;
    bool m_tracking;
    QBrush m_idleBrush, m_trackingBrush;
    QPen m_pen;
    int m_dim, m_effectiveWidth;

    void updateValueFromMouseX(const int x);

signals:
    void valueChanged(int value);

protected:
    void paintEvent(QPaintEvent *) Q_DECL_OVERRIDE;
    void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QMouseEvent *) Q_DECL_OVERRIDE;
    void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
};


// MENUBUTTON
class MenuButton : public QWidget
{
    Q_OBJECT

public:
    MenuButton(const QString & text, QWidget * parent = 0);

private:
    QBrush m_brushIdle, m_brushHovered;
    QPen m_pen;
    bool m_isHovered;

signals:
    void clicked();

protected:
    void paintEvent(QPaintEvent *) Q_DECL_OVERRIDE;
    void mousePressEvent(QMouseEvent *) Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void enterEvent(QEvent *) Q_DECL_OVERRIDE;
    void leaveEvent(QEvent *) Q_DECL_OVERRIDE;
};



// BLOOMEDLABEL
class BloomedLabel : public QLabel
{
    Q_OBJECT

public:
    BloomedLabel(QString text);
    BloomedLabel();
};



// VALUESLIDESHOW
class ArrowButton : public QPushButton
{
    Q_OBJECT

public:
    ArrowButton(bool facingleft = true, QWidget *parent = 0);

private:
    QPolygon m_arrow;
    QBrush m_brush;
    bool m_facingleft;

    void makeArrow();

protected:
    void paintEvent(QPaintEvent *) Q_DECL_OVERRIDE;
    void enterEvent(QEvent *) Q_DECL_OVERRIDE;
    void leaveEvent(QEvent *) Q_DECL_OVERRIDE;
    void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;

};


class ValueSlideshow : public QWidget
{
    Q_OBJECT

public:
    ValueSlideshow(QWidget *parent = 0);

    void addValue(const int val);
    bool setLabel(const int val, QString text);
    bool setCurrentValue(const int val);

private:
    QHash<int, QString> m_labels;

    void updateLabel();

signals:
    void valueChanged(int value);

private slots:
    void onLeftArrowClicked();
    void onRightArrowClicked();

private:
    int m_index;
    QVector<int> *m_values;
    BloomedLabel *m_displaytext;

};



// CHECKBOX
class CheckBoxBox : public QWidget
{
    Q_OBJECT

public:
    CheckBoxBox(const bool checked = false);

private:
    QPen m_pen, m_penHighlighted;
    bool m_checked, m_highlighted;

signals:
    void checkedChanged(int value);

protected:
    void paintEvent(QPaintEvent *) Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QMouseEvent *) Q_DECL_OVERRIDE;
    void enterEvent(QEvent *) Q_DECL_OVERRIDE;
    void leaveEvent(QEvent *) Q_DECL_OVERRIDE;
};


class CheckBox : public QWidget
{
    Q_OBJECT

public:
    CheckBox(QString text, const bool checked = false);

signals:
    void stateChanged(int value);

private slots:
    void onCheckedChanged(int value);
};

#endif // MENUITEMS_H
