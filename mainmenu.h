#ifndef MAINMENU_H
#define MAINMENU_H

#include "gui.h"
#include "menuitems.h"
#include "alert.h"
#include "utils.h"
#include "utils.h"
#include <QtWidgets>

#define HANDLEWIDTH 25
#define MARGIN 11
#define ANIMTIME 500

class GUI; // Forward declaration

/* Class for a sidebar menu containing all controls for game options, including
 * gameplay, graphics and audio. */
class MainMenu : public QWidget
{
    Q_OBJECT

public:
    MainMenu(GUI *parent = 0, QSize size = QSize());

    bool isOpened() const;
    void processWindowStateChange();

private:
    QPolygon m_arrow;
    QPropertyAnimation *m_animation;
    bool m_opened;

    QMediaPlayer *musicplayer;
    BloomedLabel *currenttrack;
    QString getCurrentTrackName();


private slots:
    void open();
    void close();
    void OnFinishedAnimation();
    void onTrackChanged();
    void onMusicVolumeChanged(int value);

protected:
    void paintEvent(QPaintEvent *) Q_DECL_OVERRIDE;
    void enterEvent(QEvent *) Q_DECL_OVERRIDE;
    void leaveEvent(QEvent *) Q_DECL_OVERRIDE;
};

#endif // MAINMENU_H
