#include "gui.h"

GUI::GUI(QWidget *parent, GameEngine *game)
    : QWidget(parent)
{
    // Setup alert
    m_alert = 0;

    // Setup GameEngine
    m_game = game;
    connect(game, SIGNAL(StateChanged(int)), this, SLOT(OnStateChanged(int)));
    m_boardgui = new BoardGUI(Utils::appSettings.shaking,
                            Utils::appSettings.postProcessing, game, this);

    // Setup MainMenu
    m_menu = new MainMenu(this, QSize(400, ROWS*CELLSIZE));
    m_menu->move(COLS*CELLSIZE, 0);

    // Setup timer
    m_timer = new QTimer(this);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(OnTimeout()));
    m_timer->start(1000/Utils::appSettings.maxFPS);

    // Set title and deal with size
    setWindowTitle(tr("ConneQt Four"));
    setMinimumSize(COLS*CELLSIZE+HANDLEWIDTH, ROWS*CELLSIZE);
    resize(Utils::appSettings.windowSize);

    // We will paint our background ourselves, thank you very much
    setAttribute(Qt::WA_NoSystemBackground);

#ifdef DEBUG
    debug = new Debug(this);
#endif
}


void GUI::spawnQuitAlert()
/* Spawns an Alert popup asking the user to confirm to quit. */
{
    QString msg = tr("Are you sure you want to quit?");
    m_alert = new Alert(msg, AlertButton::No, this);
    m_alert->move((this->width()-m_alert->width())/2, (this->height()-m_alert->height())/2);
    connect(m_alert, SIGNAL(yesClicked()), this, SLOT(onYesQuit()));
    connect(m_alert, SIGNAL(noClicked()), this, SLOT(onNoQuit()));
    m_alert->setFocus();
    m_alert->show();
}


void GUI::OnTimeout()
/* Slot for main timer timeout. */
{
    m_boardgui->OnTimeout();
    m_menu->update();
#ifdef DEBUG
    debug->update();
#endif
}


void GUI::onMaxFPSChanged(int value)
/* Slot for changing the refresh rate at the user's request. */
{
    // Set value
    m_timer->setInterval(1000/value);
    // Update settings file
    Utils::appSettings.maxFPS = value;
}


void GUI::onYesRestart()
/* Slot for restarting a new game at the user's request after a game over. */
{
    // Reset GameEngine and BoardGUI.
    m_game->ResetGame();
    m_boardgui->reset();

    // Delete popup
    delete m_alert;
    m_alert = 0;

    // Restore I/O children
    m_boardgui->grabKeyboard();
    m_boardgui->setEnabled(true);
    m_menu->setEnabled(true);
}


void GUI::onNoRestart()
/* Slot for closing the game at the user's request after a game over. */
{
    Utils::cleanup(m_game);
    QApplication::quit();
}


void GUI::onFullscreenChange(int state)
/* Slot for changing the window state at the user's request. */
{
    switch (state) {
    case (Qt::Checked):
        // Apply
        setWindowState(Qt::WindowFullScreen);

        // Update settings
        Utils::appSettings.fullscreen = true;
        break;
    case (Qt::Unchecked):
        // Apply
        setWindowState(Qt::WindowNoState);

        // Update settings
        Utils::appSettings.fullscreen = false;
        break;
    default:
        // No-op
        ;
    }
}


void GUI::onMaxpliesChanged(int value)
/* Slot for changing GameEngine's minimax cutoff depth at the user's request. */
{
    // Apply
    m_game->setMaxplies(value);

    // Update settings
    Utils::appSettings.difficulty = value;
}


void GUI::onShakingChanged(int state)
/* Slot for toggling BoardGUI's shaking animation at the user's request. */
{
    switch (state) {
    case (Qt::Checked):
        // Apply
        m_boardgui->enableShaking(true);

        // Update settings
        Utils::appSettings.shaking = true;
        break;
    case (Qt::Unchecked):
        // Apply
        m_boardgui->enableShaking(false);

        // Update settings
        Utils::appSettings.shaking = false;
        break;
    default:
        // No-op
        ;
    }
}

void GUI::onPostprocessingChanged(int state)
/* Slot for toggling BoardGUI's post-processing at the user's request. */
{
    switch (state) {
    case (Qt::Checked):
        // Apply
        m_boardgui->enablePostprocessing(true);

        // Update settings
        Utils::appSettings.postProcessing = true;
        break;
    case (Qt::Unchecked):
        // Apply
        m_boardgui->enablePostprocessing(false);

        // Update settings
        Utils::appSettings.postProcessing = false;
        break;
    default:
        // No-op
        ;
    }
}


void GUI::onSFXVolumeChanged(int value)
/* Slot for changing the sound effects volume at the user's request. */
{
    // Set value
    m_boardgui->setSFXVolume(value/100.0);

    // Update settings
    Utils::appSettings.sfxVolume = value;
}


void GUI::OnStateChanged(const int state)
/* Slot for a state change in GameEngine. We only care about "game over" states. */
{
    if (m_alert == 0) {
        // No alerts open already --though there should never be any!
        QString msg;

        switch (state) {
        case (STATE_P1WIN):
            msg += tr("YOU WON!");
            break;
        case (STATE_P2WIN):
            msg += tr("YOU LOST...");
            break;
        case (STATE_TIE):
            msg = tr("TIE.");
            break;
        default:
            // Do nothing in all other states.
            return;
        }
        m_alert = new Alert(msg, tr("\nWould you like to play again?"),
                            AlertButton::Yes, this);
        m_alert->move((this->width()-m_alert->width())/2,
                      (this->height()-m_alert->height())/2);
        connect(m_alert, SIGNAL(yesClicked()), this, SLOT(onYesRestart()));
        connect(m_alert, SIGNAL(noClicked()), this, SLOT(onNoRestart()));
        m_alert->show();

        // Disable other gui components so they don't get I/O events.
        m_boardgui->setEnabled(false);
        m_menu->setEnabled(false);
    }
}

void GUI::onQuitRequest()
/* Slot for opening a quit Alert at the user's request. */
{
    if (m_alert == 0) {
        // No alerts open already --though there should never be any!
        spawnQuitAlert();

        // Disable other gui components so they don't get I/O events.
        m_boardgui->setEnabled(false);
        m_menu->setEnabled(false);
    }
}


void GUI::onYesQuit()
/* Slot for closing the game from a quit Alert at the user's confirmation. */
{
    Utils::cleanup(m_game);
    QApplication::quit();
}


void GUI::onNoQuit()
/* Slot for resuming the game from a quit Alert at the user's confirmation. */
{
    if (m_alert) {
        delete m_alert;
        m_alert = 0;
        m_boardgui->grabKeyboard();
        m_boardgui->setEnabled(true);
        m_menu->setEnabled(true);
    }
}


void GUI::paintEvent(QPaintEvent *)
/* We will draw a custom background. */
{
    QPainter p(this);
    // It'll end up as black bands.
    p.fillRect(this->rect(), this->palette().base());
}


void GUI::resizeEvent(QResizeEvent *event)
{
    // Resize board, which has a fixed aspect ratio
    qreal targetratio = (qreal)COLS/(qreal)ROWS; // w/h
    qreal eventratio =(qreal)(event->size().width()-HANDLEWIDTH) /
                      (qreal)event->size().height();
    int w,h;

    if (eventratio <= targetratio) {
        // Too narrow, bands above and below
        w = event->size().width()-HANDLEWIDTH;
        h = w/targetratio;
    } else {
        // Too short, bands on the sides
        h = event->size().height();
        w = h*targetratio;
    }
    m_boardgui->resize(w, h);
    m_boardgui->move((event->size().width()-HANDLEWIDTH-w)/2,(event->size().height()-h)/2);

    // Main menu doesn't care about proportions, it's just as tall as the window
    if (m_menu->isOpened()) {
        m_menu->move(event->size().width()-m_menu->width(), 0);
    } else {
        m_menu->move(event->size().width()-HANDLEWIDTH, 0);
    }
    m_menu->resize(m_menu->width(), event->size().height());

    // Update settings
    if (this->windowState() == Qt::WindowNoState) {
        // The window is neither fullscreen, maximized or minimized
        Utils::appSettings.windowSize = event->size();
    }
}


void GUI::changeEvent(QEvent *event)
// Triggered whenever there is a "state change" in the widget.
{
    switch(event->type()) {
    case (QEvent::WindowStateChange):
        /* WindowState is maximized, fullscreen etc.
         * We care because menu needs to be notified in case it was mid-animation */
        m_menu->processWindowStateChange();

        // Update settings with maximized status
        Utils::appSettings.maximized = this->isMaximized();
        break;
    default:
        QWidget::changeEvent(event);
    }

}


void GUI::closeEvent(QCloseEvent *)
/* Triggered by exit() commands from the OS, like hitting the cross button in
 * the title bar or Alt+F4. */
{
    Utils::cleanup(m_game);
}
