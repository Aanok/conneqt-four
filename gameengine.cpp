#include "gameengine.h"
#include <iostream>


GameEngine::GameEngine()
    : QThread()
{
    /* Since BoardGUI polls us at every frame, we have to separate a "public",
     * stable board where we only write actual moves and a "private" workboard,
     * which we use as a cursor to explore the state space. */
    m_workboard = new int[ROWS*COLS];
    m_board = new int[ROWS*COLS];
#ifdef DEBUG
    // Initializes both boards to the same random values.
    int i, j;
    for (i = 0; i < ROWS; i++) {
        for (j = 0; j < COLS; j++) {
            switch (qrand() % 3) {
                case 0:
                    SetTok(i, j, TOKEMPTY);
                    SetWorkTok(i, j, TOKEMPTY);
                    break;
                case 1:
                    SetTok(i, j, TOKP1);
                    SetWorkTok(i, j, TOKP1);
                    break;
                case 2:
                    SetTok(i, j, TOKP2);
                    SetWorkTok(i, j, TOKP2);
            }
        }
    }
#else
    // Initialize both boards to empty.
    std::memset(m_workboard, 0, ROWS*COLS*sizeof(int));
    std::memset(m_board, 0, ROWS*COLS*sizeof(int));
#endif
    m_maxplies = Utils::appSettings.difficulty;
    m_gamestate = STATE_RUNNING;
    m_gameturn = 0;
}


int GameEngine::GetTok(const int row, const int col)
/* REQUIRES: 0 <= row < ROWS; 0 <= col < COLS.
 * EFFECT: returns intger handle (TOKP1, TOKP2 o TOKEMPTY) of element at cell [row][col] of public board. */
{
    return m_board[row*COLS +col];
}


int GameEngine::GetWorkTok(const int row, const int col)
/* REQUIRES: 0 <= row < ROWS; 0 <= col < COLS.
 * EFFECT: returns intger handle (TOKP1, TOKP2 o TOKEMPTY) of element at cell [row][col] of workboard. */
{
    return m_workboard[row*COLS +col];
}


int GameEngine::GetFirstFreeRow(int j)
/* REQUIRES: 0 <= j < COLS */
{
    int i = -1;
    while (i+1 < ROWS && GetTok(i+1,j) == TOKEMPTY) {
        i++;
    }
    return i;
}


int GameEngine::GetFirstFreeWorkRow(int j)
/* REQUIRES: 0 <= j < COLS */
{
    int i = -1;
    while (i+1 < ROWS && GetWorkTok(i+1,j) == TOKEMPTY) {
        i++;
    }
    return i;
}


void GameEngine::MakeP1Move(const int row, const int col)
/* REQUIRES: 0 <= row < ROWS, 0 <= cols <= COLS.
 * EFFECT: places a TOKP1 at logic cell [row][col] of both boards, advances turn,
 * triggers P1 winstate if appropriate. */
{
    SetWorkTok(row, col, TOKP1);
    SetTok(row, col, TOKP1);
    AdvanceTurn();
    if (IsWinMove(row, col)) {
        SetState(STATE_P1WIN);
    }
}


std::pair<int,int> GameEngine::MakeCPUMove()
/* REQUIRES: game standing at P2's turn.
 * EFFECT: makes the best move for P2 on both boards. */
{
    int i, j, tmp, bestmove;
    int freemoves[COLS];
    int maxscore = INT_MIN;
    bool go = true;

    /* First pass: if there's an immediate winning move, we do it right away.
     * This makes the AI look much more attentive and responsive. */
    j = 0;
    while (go && j < COLS) {
        if ((i = GetFirstFreeWorkRow(j)) != -1) {
            // It's a valid move
            SetWorkTok(i, j, TOKP2);
            if (IsWinMove(i,j)) {
                // An immediate winning move
                bestmove = j;
                go = false;
            }
            // Revert the move
            SetWorkTok(i, j, TOKEMPTY);
        }
        // Advance column
        j++;
    }

    /* If the first pass failed (thus go == true), we do actual minimax
     * expansion, in random order. */
    if (go) {
        // Init
        for (i = 0 ; i < COLS; i++) {
            freemoves[i] = i;
        }
        // Shuffle
        for (i = 0; i < COLS; i++) {
            // freemoves[i] = i, in principle
            if ((j = qrand() % COLS) != (tmp = qrand() % COLS)) {
                freemoves[i] ^= freemoves[tmp];
                freemoves[tmp] ^= freemoves[i];
                freemoves[i] ^= freemoves[tmp];
            }
        }
    }
    // Perform randomized lookup
    j = 0;
    while (go && j < COLS) {
        if ((i = GetFirstFreeWorkRow(freemoves[j])) != -1) {
            // It's a valid move
            SetWorkTok(i, freemoves[j], TOKP2);
            // Minimax lookup
            if ((tmp = MinMove(INT_MIN, INT_MAX, 2)) >= maxscore) {
                // Move is not worse than what we had; we keep it
                bestmove = freemoves[j];
                maxscore = tmp;
                if (tmp == INT_MAX) {
                    // A winning move; we stop right away
                    go = false;
                }
            }
            // Revert the move
            SetWorkTok(i, freemoves[j], TOKEMPTY);
        }
        // Next column
        j++;
    }

    /* Commit the move to the "public" board.
     * NB: if all moves are losing, we're effectively acting at random.
     * Which is good. */
    i = GetFirstFreeWorkRow(bestmove);
    SetWorkTok(i, bestmove, TOKP2);
    SetTok(i, bestmove, TOKP2);
    if (IsWinMove(i, bestmove)) {
        SetState(STATE_P2WIN);
    } else {
        AdvanceTurn();
    }

    // We return the logical coordinates of the move to BoardGUI.
    return std::pair<int,int>(i, bestmove);
}


void GameEngine::OnCommandCPUMove()
/* EFFECT: slot to trigger minimax search for CPU move.
 * If in correct state, wakes up the thread. */
{
    if (m_gamestate == STATE_RUNNING) {
        m_condition.wakeOne();
    }
}


void GameEngine::run()
/* EFFECT: thread for executing minimax search for CPU move. Stays dormant
 * most of the time, is woken up by makeCPUMove on a signal.
 * EMITS signal to notify whomever cares about it (BoardGUI) of the new move made. */
{
    std::pair<int,int> move;
    forever {
        m_mutex.lock();
        m_condition.wait(&m_mutex);
        m_mutex.unlock();
        if (m_gamestate == STATE_QUITTING) return; // We're quitting the app.
        if (m_gameturn != 0) {
            // CPU never starts, but gameturn can be 0 if we just reset the game.
            move = MakeCPUMove();
            emit MadeCPUMove(move.first, move.second);
        }
    }
}


void GameEngine::ResetGame()
/* EFFECT: resets the GameEngine instance to the beginning of a new game. */
{
    std::memset(m_workboard, 0, ROWS*COLS*sizeof(int));
    std::memset(m_board, 0, ROWS*COLS*sizeof(int));
    m_gameturn = 0;
    SetState(STATE_RUNNING);
}


void GameEngine::QuitGame()
/* EFFECT: allows graceful shutdown by waking the minimax thread, which then exits. */
{
    SetState(STATE_QUITTING);
    m_condition.wakeOne();
}


void GameEngine::setMaxplies(const int value)
/* EFFECT: changes the minimax lookahead depth. */
{
    m_maxplies = value > 10 ? value : 10;
}


void GameEngine::SetState(const int state)
/* EFFECT: sets the state to the argument.
 * EMITS a signal to notify whomever cares about it (GUI). */
{
    m_gamestate = state;
    emit StateChanged(state);
}


void GameEngine::AdvanceTurn()
/* EFFECT: advances game turn and checks if the board is full without standing at a winstate,
 * which signifies a tie. Sets state to STATE_TIE if need be.
 * NB: IsWinState has already been invoked after the latest move when this is called. */
{
    if (++m_gameturn == ROWS*COLS && m_gamestate == STATE_RUNNING)
    {
        SetState(STATE_TIE);
    }
}


void GameEngine::SetTok(const int row, const int col, const int tok)
/* REQUIRES: tok = TOKP1 or TOKP2 or TOKEMPTY; 0 <= row < ROWS; 0 <= col < COLS.
 * EFFECT: sets the logical cel [row][col] of public board to token tok. */
{
    m_board[row*COLS +col] = tok;
}


void GameEngine::SetWorkTok(int row, int col, int tok)
/* REQUIRES: tok = TOKP1 or TOKP2 or TOKEMPTY; 0 <= row < ROWS; 0 <= col < COLS.
 * EFFECT: sets the logical cel [row][col] of workboard to token tok. */
{
    m_workboard[row*COLS +col] = tok;
}


bool GameEngine::IsWinMove(int row, int col) {
/* REQUIRES: 0 <= row < ROWS, 0 <= col < COLS, GetTok(row,col) != TOKEMPTY.
 * EFFECT: determines if the token at logical coordinates [row][col] on the
 * workboard triggers a victory for the corresponding player.
 * The function is meant to be called after making a move, to see if it triggers
 * a winstate.
 * RETURNS 1 for victory, 0 otherwise. */
    int i, j, streak;
    int token = GetWorkTok(row,col);

    /* Central cell is counted twice, so streak starts from -1.
     * All directions in the following code and comments are meant as logical.
     * Diagonal, upwards. */
    streak = -1;
    i = row;
    j = col;
    while (0 <= i && i < ROWS && 0 <= j && j < COLS) {
        if (GetWorkTok(i,j) == token) {
            streak++;
            i--;
            j--;
        } else {
            break;
        }
    }
    /* Diagonal, downwards. */
    i = row;
    j = col;
    while (0 <= i && i < ROWS && 0 <= j && j < COLS) {
        if (GetWorkTok(i,j) == token) {
            streak++;
            i++;
            j++;
        } else {
            break;
        }
    }
    if (streak >= 4) {
        return true;
    }

    /* Antidiagonal, upwards. */
    streak = -1;
    i = row;
    j = col;
    while (0 <= i && i < ROWS && 0 <= j && j < COLS) {
        if (GetWorkTok(i,j) == token) {
            streak++;
            i--;
            j++;
        } else {
            break;
        }
    }
    /* Antidiagonal, downwards. */
    i = row;
    j = col;
    while (0 <= i && i < ROWS && 0 <= j && j < COLS) {
        if (GetWorkTok(i,j) == token) {
            streak++;
            i++;
            j--;
        } else {
            break;
        }
    }
    if (streak >= 4) {
        return true;
    }

    /* Horizontal, leftwards. */
    streak = -1;
    j = col;
    while (0 <= j && j < COLS) {
        if (GetWorkTok(row,j) == token) {
            streak++;
            j--;
        } else {
            break;
        }
    }
    /* Horizontal, rightwards. */
    j = col;
    while (0 <= j && j < COLS) {
        if (GetWorkTok(row,j) == token) {
            streak++;
            j++;
        } else {
            break;
        }
    }
    if (streak >= 4) {
        return true;
    }

    /* Vertical, upwards is not necessary so streak starts from 0 this time.
     * Vertical, downwards. */
    streak = 0;
    i = row;
    while (0 <= i && i < ROWS) {
        if (GetWorkTok(i,col) == token) {
            streak++;
            i++;
        } else {
            break;
        }
    }
    if (streak >= 4) {
        return true;
    }

    /* The token at logical coordinates [row][col] dows not determine a victory. */
    return false;
}


int GameEngine::PairHeuristic() {
/* RETURNS: heuristic evaluation of current gamestate. Counts two points per
 * neighbouring pair of TOKP2 in any eight (logic) directions.
 * This favours states with clusters or lines of tokens. */
    int i, j;
    int retval = 0;

    /* To avoid checking if accesses are legal (because of logic borders),
     * we'll split the board in sections treated separately.
     * All checks are listed clockwise from 12 hours. */

    /* Top-right corner */
    if (GetWorkTok(0,COLS-1) == TOKP2) {
        retval += (GetWorkTok(1,COLS-1) == TOKP2) + (GetWorkTok(1,COLS-2) == TOKP2)
                + (GetWorkTok(0,COLS-2) == TOKP2);
    }

    /* Bottom-right corner */
    if (GetWorkTok(ROWS-1,COLS-1) == TOKP2) {
        retval += (GetWorkTok(ROWS-2,COLS-1) == TOKP2) + (GetWorkTok(ROWS-1,COLS-2) == TOKP2)
                + (GetWorkTok(ROWS-2,COLS-2) == TOKP2);
    }

    /* Bottom-left corner */
    if (GetWorkTok(ROWS-1,0) == TOKP2) {
        retval += (GetWorkTok(ROWS-2,0) == TOKP2) + (GetWorkTok(ROWS-2,1) == TOKP2)
                + (GetWorkTok(ROWS-1,1) == TOKP2);
    }

    /* Top-left corner */
    if (GetWorkTok(0,0) == TOKP2) {
        retval += (GetWorkTok(0,1) == TOKP2) + (GetWorkTok(1,1) == TOKP2)
                + (GetWorkTok(1,0) == TOKP2);
    }

    /* Top row */
    for (j = 1; j < COLS-1; j++) {
        if (GetWorkTok(0,j) == TOKP2) {
            retval += (GetWorkTok(0,j+1) == TOKP2) + (GetWorkTok(1,j+1) == TOKP2)
                    + (GetWorkTok(1,j) == TOKP2) + (GetWorkTok(1,j-1) == TOKP2)
                    + (GetWorkTok(0,j-1) == TOKP2);
        }
    }

    /* Right column */
    for (i = 1; i < ROWS-1; i++) {
        if (GetWorkTok(i,COLS-1) == TOKP2) {
            retval += (GetWorkTok(i-1,COLS-1) == TOKP2) + (GetWorkTok(i+1,COLS-1) == TOKP2)
                    + (GetWorkTok(i+1,COLS-2) == TOKP2) + (GetWorkTok(i,COLS-2) == TOKP2)
                    + (GetWorkTok(i-1,COLS-2) == TOKP2);
        }
    }

    /* Bottom row */
    for (j = 1; j < COLS-1; j++) {
        if (GetWorkTok(ROWS-1,j) == TOKP2) {
            retval += (GetWorkTok(ROWS-2,j) == TOKP2) + (GetWorkTok(ROWS-2,j+1) == TOKP2)
                    + (GetWorkTok(ROWS-1,j+1) == TOKP2) + (GetWorkTok(ROWS-1,j-1) == TOKP2)
                    + (GetWorkTok(ROWS-2,j-1) == TOKP2);
        }
    }

    /* Left column */
    for (i = 1; i < ROWS-1; i++) {
        if (GetWorkTok(i,0) == TOKP2) {
            retval += (GetWorkTok(i-1,0) == TOKP2) + (GetWorkTok(i-1,1) == TOKP2)
                    + (GetWorkTok(i,1) == TOKP2) + (GetWorkTok(i+1,1) == TOKP2)
                    + (GetWorkTok(i+1,0) == TOKP2);
        }
    }

    /* Center */
    for (i = 1; i < ROWS-1; i++) {
        for (j = 1; j < COLS-1; j++) {
            if (GetWorkTok(i,j) == TOKP2) {
                retval += (GetWorkTok(i-1,j) == TOKP2) + (GetWorkTok(i-1,j+1) == TOKP2)
                        + (GetWorkTok(i,j+1) == TOKP2) + (GetWorkTok(i+1,j+1) == TOKP2)
                        + (GetWorkTok(i+1,j) == TOKP2) + (GetWorkTok(i+1,j-1) == TOKP2)
                        + (GetWorkTok(i,j-1) == TOKP2) + (GetWorkTok(i-1,j-1) == TOKP2);
            }
        }
    }

    return retval;
}


int GameEngine::MaxMove(int a, int b, int ply)
/* REQUIRES: a, b valid alpha-beta parameters, current state corresponding to MAX player.
 * EFFECT: computes a MAX evaluation of the current state on the workboard.
 * RETURNS: score for the current state. */
{
    int i, j, tmp;
    bool found = false;
    int maxscore = INT_MIN;

    if (ply >= m_maxplies) {
        // Cutoff depth reached.
        return PairHeuristic();
    }

    for (j = 0; j < COLS; j++) {
        if ((i = GetFirstFreeWorkRow(j)) != -1) {
            // The move is valid.
            found = true;
            SetWorkTok(i, j, TOKP2);
            if (IsWinMove(i,j)) {
                // An immediate winning move.
                SetWorkTok(i, j, TOKEMPTY);
                return INT_MAX;
            }
            // Not an immediate winning move; minimax lookup required.
            if ((tmp = MinMove(a, b, ply+1)) > maxscore) {
                maxscore = tmp;
                // Alpha-beta pruning
                if (tmp > a) {
                    a = tmp;
                    if (b <= a) {
                        SetWorkTok(i, j, TOKEMPTY);
                        return maxscore;
                    }
                }
            }
            SetWorkTok(i, j, TOKEMPTY);
        }
    }

    // found == false means the board is full (a tie)
    return found ? maxscore : TIEVAL;
}


int GameEngine::MinMove(int a, int b, int ply)
/* REQUIRES: a, b valid alpha-beta parameters, current state corresponding to MIN player.
 * EFFECT: computes a MIN evaluation of the current state on the workboard.
 * RETURNS: score for the current state. */
{
    int i, j, tmp;
    bool found = false;
    int minscore = INT_MAX;

    if (ply >= m_maxplies) {
        // Cutoff depth reached.
        return PairHeuristic();
    }

    for (j = 0; j < COLS; j++) {
        if ((i = GetFirstFreeWorkRow(j)) != -1) {
            // The move is valid.
            found = true;
            SetWorkTok(i, j, TOKP1);
            if (IsWinMove(i,j)) {
                // An immediate winning move.
                SetWorkTok(i, j, TOKEMPTY);
                return INT_MIN;
            }
            // Not an immediate winning move; minimax lookup required.
            if ((tmp = MaxMove(a, b, ply+1)) < minscore) {
                minscore = tmp;
                // Alpha-beta pruning
                if (tmp < b) {
                    b = tmp;
                    if (b <= a) {
                        SetWorkTok(i, j, TOKEMPTY);
                        return minscore;
                    }
                }
            }
            SetWorkTok(i, j, TOKEMPTY);
        }
    }

    // found == false means the board is full (a tie)
    return found ? minscore : TIEVAL;
}
