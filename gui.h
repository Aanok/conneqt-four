#ifndef GUI_H
#define GUI_H

#include <QtWidgets>
#include <QtMultimedia>
#include "gameengine.h"
#include "boardgui.h"
#include "mainmenu.h"
#include "debug.h"
#include "alert.h"
#include "utils.h"
class MainMenu; // Forward declaration because lel c++
class BoardGUI; // BOY HOWDY WHAT AN ELEGANT LANGUAGE

/* Main UI class for the whole application. Deals with resizing, alerts and
 * interaction between components. Also houses the main timer for animations. */
class GUI : public QWidget
{
    Q_OBJECT

public:
    GUI(QWidget *parent = 0, GameEngine *game = 0);

private:
    GameEngine *m_game;
    BoardGUI *m_boardgui;
    MainMenu *m_menu;
#ifdef DEBUG
    Debug *debug;
#endif

    QTimer *m_timer;

    Alert *m_alert;

    void spawnQuitAlert();

private slots:
    void OnStateChanged(const int state);
    void OnTimeout();
    void onYesRestart();
    void onNoRestart();
    void onFullscreenChange(int state);

public slots:
    void onMaxFPSChanged(int value);
    void onMaxpliesChanged(int value);
    void onShakingChanged(int state);
    void onPostprocessingChanged(int state);
    void onSFXVolumeChanged(int value);
    void onQuitRequest();
    void onYesQuit();
    void onNoQuit();

protected:
    void paintEvent(QPaintEvent *) Q_DECL_OVERRIDE;
    void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
    void changeEvent(QEvent *event) Q_DECL_OVERRIDE;
    void closeEvent(QCloseEvent *) Q_DECL_OVERRIDE;
};

#endif // GUI_H
