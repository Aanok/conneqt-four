#ifdef DEBUG
#ifndef DEBUG_H
#define DEBUG_H

#include <QWidget>
#include <QPainter>
#include <QTime>

/* Utility class for debugging, compiled only if -DDEBUG is passed to the
 * compiler. Just prints the milliseconds since last frame update. */
class Debug : public QWidget
{
public:
    Debug(QWidget *parent = 0);

private:
    int m_lasttime;
    QFont m_textfont;
    QPen m_textpen;

protected:
    void paintEvent(QPaintEvent *) Q_DECL_OVERRIDE;
};

#endif // DEBUG_H
#endif // DEBUG
